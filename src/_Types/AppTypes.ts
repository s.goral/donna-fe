import { Gender } from '_Types/types';

export interface IStatistics {
  totalReservations?: number;
  canceledReservations: number;
  finishedReservations: number;
  newReservations: number;
}

export interface IClient {
  id?: number;
  name: string;
  lastName: string;
  joinDate?: string;
  birthday: string;
  phoneNumber: string;
  email: string;
  gender?: Gender;
}

export interface IPersonData {
  name: string;
  lastName: string;
  reservations: IReservationDTO[];
}

export interface ICompany {
  id?: number;
  name: string;
  clientsAmount: number;
  employeesAmount: number;
}

export interface IUser {
  id?: number;
  name?: string;
  lastName?: string;
  email?: string;
  roleName?: RoleName;
  active?: boolean;
}

export enum RoleName {
  ROLE_ADMIN = 'ROLE_ADMIN',
  ROLE_USER = 'ROLE_USER',
  ROLE_USER_ADMIN = 'ROLE_USER_ADMIN',
}

export interface IEmployee {
  id?: number;
  name: string;
  lastName: string;
}

export interface IReservationDTO {
  id?: number;
  clientName: string;
  clientId: number;
  startDate: string;
  endDate: string;
  ownerId?: number;
  ownerName: string;
  service: string;
  status: ReservationStatus;
}

export interface IReservation {
  id?: number;
  title: string;
  startDate: string;
  endDate: string;
  status: ReservationStatus;
}

export enum ReservationStatus {
  CANCELED = 'CANCELED',
  NEW = 'NEW',
  FINISHED = 'FINISHED',
}
