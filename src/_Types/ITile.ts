import { ReactNode } from 'react';

export interface ITile {
  label?: string;
  amount?: any;
  component?: ReactNode;
}
