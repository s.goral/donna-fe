import React, { FC } from 'react';
import { ThemeProvider } from 'emotion-theming';
import { Global } from '@emotion/core';
import { Provider } from 'react-redux';
import { globalStyles } from '_Styles/globalStyles';
import { MuiThemeProvider } from '@material-ui/core';
import { ICommonProps } from '_Types/props';
import { store } from '_State/store';
import { theme } from '_Styles/theme';

export const GlobalProviders: FC<ICommonProps> = ({ children }) => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <MuiThemeProvider theme={theme}>
          <Global styles={globalStyles} />
          {children}
        </MuiThemeProvider>
      </ThemeProvider>
    </Provider>
  );
};
