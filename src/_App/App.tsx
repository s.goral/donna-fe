import React, { useEffect } from 'react';
import '_App/App.css';
import { GlobalProviders } from '_App/GlobalProviders';
import { BrowserRouter } from 'react-router-dom';
import styled from '@emotion/styled';
import { IThemed } from '_Styles/types';
import { RoutesSwitcher } from '_Routes/RoutesSwitcher';
import { setCredentials } from '_Api/api';

const MainScene = styled.div<IThemed>`
  display: flex;
  height: 100%;
  background-color: ${({ theme }) => theme.palette.background.default};
`;

const MainContainer = styled.div<IThemed>`
  font-size: 1.4rem;
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  background-color: ${({ theme }) => theme.palette.background.default};
`;

const App = () => {
  const token = sessionStorage.getItem('access_token');

  useEffect(() => {
    setCredentials({ token });
  }, [token]);

  return (
    <GlobalProviders>
      <BrowserRouter>
        <MainScene>
          <MainContainer>
            <RoutesSwitcher />
          </MainContainer>
        </MainScene>
      </BrowserRouter>
    </GlobalProviders>
  );
};

export default App;
