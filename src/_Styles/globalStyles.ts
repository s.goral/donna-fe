import { css } from '@emotion/core';

export const globalStyles = css`
  html,
  body,
  div#root {
    height: 100%;
    background: #f2f2f2;
    *::placeholder {
      color: #abb1b6;
      opacity: 1;
    }
  }

  body {
    font-family: Roboto, sans-serif;
  }
`;
