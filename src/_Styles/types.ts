import { Theme } from '@material-ui/core';

export interface IThemed {
  theme: Theme;
}
