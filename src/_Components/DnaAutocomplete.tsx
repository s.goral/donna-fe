import React, { FC, useEffect, useState } from 'react';
import { Autocomplete, AutocompleteChangeReason } from '@material-ui/lab';
import { TextField } from '@material-ui/core';
import styled from '@emotion/styled';
import { isNil } from 'lodash';

export const OptionWrapper = styled.div`
  height: 6.5rem;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

export const OptionLabel = styled.div`
  margin: auto 0;
`;

interface IProps {
  value?: any;
  data?: any[];
  renderOption?: (option: any) => React.ReactNode;
  getOptionLabel?: (option: any) => string;
  onChange?: (value: any) => void;
  label?: string;
  defaultValue?: any;
  valueIndex?: number;
}

export const DnaAutocomplete: FC<IProps> = ({
  value,
  data,
  renderOption,
  getOptionLabel,
  onChange,
  label,
  defaultValue,
}) => {
  const handleChange = (
    event: React.ChangeEvent<{}>,
    newValue: any,
    reason: AutocompleteChangeReason
  ) => {
    if (onChange) onChange(newValue);
  };
  return (
    <Autocomplete
      value={value}
      freeSolo
      onChange={handleChange}
      options={data}
      getOptionLabel={
        getOptionLabel ? getOptionLabel : (o) => `${o.name} ${o.lastName}`
      }
      renderOption={
        renderOption
          ? renderOption
          : (option) => `${option.name} ${option.lastName}`
      }
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
        />
      )}
    />
  );
};
