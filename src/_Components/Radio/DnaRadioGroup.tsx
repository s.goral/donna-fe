import React, { ChangeEvent, FC } from 'react';
import Radio from '@material-ui/core/Radio';
import {
  createMuiTheme,
  FormControlLabel,
  MuiThemeProvider,
} from '@material-ui/core';
import RadioGroup from '@material-ui/core/RadioGroup';

export const myTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#b2857c',
    },
  },
  overrides: {
    MuiSvgIcon: {
      root: {
        color: '#b2857c',
        fontSize: '2rem',
      },
    },
    MuiTypography: {
      root: {
        fontSize: '1.5rem',
      },
      body1: {
        fontSize: '1.5rem',
      },
    },
  },
});

interface IProps {
  value: string;
  onChange: (event: ChangeEvent<HTMLInputElement>, value: string) => void;
  options: {
    value: string;
    label: string;
  }[];
}

export const DnaRadioGroup: FC<IProps> = ({ value, onChange, options }) => {
  return (
    <MuiThemeProvider theme={myTheme}>
      <RadioGroup value={value} onChange={onChange}>
        {options.map(({ value, label }, index) => {
          return (
            <FormControlLabel
              key={index}
              value={value}
              control={<Radio />}
              label={label}
            />
          );
        })}
      </RadioGroup>
    </MuiThemeProvider>
  );
};
