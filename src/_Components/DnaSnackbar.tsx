import React, { FC } from 'react';
import { Snackbar } from '@material-ui/core';
import styled from '@emotion/styled';
import { IThemed } from '_Styles/types';
import { themeGetters } from '_Styles/themeHelpers';

const { color } = themeGetters;

const Error = styled.div<IThemed>`
  width: 60rem;
  height: 5rem;
  background-color: ${color('error')};
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
    0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12);
  color: #fff;
  font-weight: 500;
  font-size: 1.8rem;
  border-radius: 6px;
`;

const Success = styled.div<IThemed>`
  width: 60rem;
  height: 5rem;
  background-color: #558055;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
    0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12);
  color: #fff;
  font-weight: 500;
  font-size: 1.8rem;
  border-radius: 6px;
`;
interface IProps {
  open: boolean;
  onClose: () => void;
  autoHideDuration?: number;
  message: string;
  type: 'error' | 'success';
}
export const DnaSnackbar: FC<IProps> = ({
  open,
  onClose,
  type,
  message,
  autoHideDuration,
}) => {
  return (
    <Snackbar
      open={open}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
    >
      {type === 'error' ? (
        <Error>{message}</Error>
      ) : (
        <Success>{message}</Success>
      )}
    </Snackbar>
  );
};
