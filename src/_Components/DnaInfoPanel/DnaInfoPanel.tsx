import React, { FC } from 'react';
import styled from '@emotion/styled';
import { IThemed } from '_Styles/types';
import { themeGetters } from '_Styles/themeHelpers';
import { Divider } from '@material-ui/core';
import { ICommonProps } from '_Types/props';

const { color } = themeGetters;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: white;
  box-shadow: 0px 4px 16px rgba(97, 91, 136, 0.15);
  width: 100%;
  height: 100%;
`;

const LabelWrapper = styled.div`
  display: inherit;
`;

const Header = styled.div<IThemed & { withButton?: boolean }>`
  background-color: #e2d7d5;
  color: ${color('primary', 'contrastText')};
  font-size: 1.6rem;
  width: 100%;
  display: flex;
  align-items: center;
  padding: ${({ withButton }) =>
    withButton ? '1rem 3.5rem' : '2.3rem 3.5rem'};
  justify-content: space-between;
  white-space: nowrap;
`;

const MainDivider = styled(Divider)`
  && {
    width: 100%;
    background-color: #eaeef1;
  }
`;

const ContentWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  overflow: auto;
  flex-direction: column;
  justify-content: space-between;
`;

export interface IInfoPanelProps extends ICommonProps {
  label: string;
  editable?: boolean;
  tooltip?: string;
  onEditClick?: () => void;
  withButton?: boolean;
  buttonLabel?: string;
  onButtonClick?: () => void;
}

export const DnaInfoPanel: FC<IInfoPanelProps> = ({
  label,
  children,
  tooltip,
  className,
  withButton,
}) => {
  return (
    <Wrapper className={className}>
      <Header withButton={withButton}>
        <LabelWrapper>{label}</LabelWrapper>
      </Header>
      <MainDivider />
      <ContentWrapper>{children}</ContentWrapper>
    </Wrapper>
  );
};
