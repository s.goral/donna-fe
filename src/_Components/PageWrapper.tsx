import React from 'react';
import styled from '@emotion/styled';
import { themeGetters } from '_Styles/themeHelpers';
import { IThemed } from '_Styles/types';

const { color } = themeGetters;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  margin: 5rem 10%;
`;

const InfoText = styled.div`
  margin-bottom: 1rem;
  display: flex;
  flex-direction: column;
  font-weight: 300;
  color: ${color('grey', 300)};
  font-size: 16px;
`;

const Header = styled.div<IThemed>`
  display: flex;
  flex-direction: column;
  font-size: 24px;
  font-weight: 500;
  color: ${color('primary', 'contrastText')};
  margin-bottom: 8rem;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;

export const PageWrapper = ({ title, titleText, children }) => {
  return (
    <Wrapper>
      <Header>
        <InfoText>{titleText}</InfoText>
        {title}
      </Header>
      <Content>{children}</Content>
    </Wrapper>
  );
};
