import React, { FC } from 'react';
import styled from '@emotion/styled';
import ReactEcharts from 'echarts-for-react';

const DoughnutChartBody = styled.div`
  overflow: hidden;
  width: 100%;
  margin: auto;
  .doughnut-chart {
    height: 130px !important;
  }
`;
interface IProps {
  option: any;
}

export const DnaDoughnutChart: FC<IProps> = ({ option }) => {
  return (
    <DoughnutChartBody>
      <ReactEcharts
        notMerge={true}
        className={'doughnut-chart'}
        option={option}
      />
    </DoughnutChartBody>
  );
};
