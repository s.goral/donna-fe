export interface IPieChartOption {
  series: any[];
  tooltip?: unknown;
}
