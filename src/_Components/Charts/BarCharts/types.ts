export interface IDatasetBarOptions {
  dataset: IDataset;
  xAxis?: IxAxis;
  yAxis?: IyAxis;
  grid?: IChartGrid;
  tooltip?: unknown;
  series: any[];
}

export interface ISimpleBarOptions {
  xAxis?: IxAxis;
  yAxis?: IyAxis;
  grid?: IChartGrid;
  tooltip?: unknown;
  series: any[];
}

export interface IChartGrid {
  left: number;
  right: number;
  top: number;
  bottom: number;
}

export interface IDataset {
  dimensions: string[];
  source: any[];
}

export interface ILegend {
  color: string;
  label: string;
}

export interface IAxis {
  show?: boolean;
}

export interface IxAxis {
  type: string;
  axisTick?: IAxis;
  axisLine?: IAxis;
  data?: string[];
}
export interface IyAxis {
  type?: string;
  axisTick?: IAxis;
  axisLine?: IAxis;
}

export interface ISeries {
  type: string;
  barWidth: number;
  itemStyle: any;
}
