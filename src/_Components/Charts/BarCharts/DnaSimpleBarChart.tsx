import React, { FC, useEffect, useRef, useState } from 'react';
import styled from '@emotion/styled';
import ReactEcharts from 'echarts-for-react';
import { useSelector } from 'react-redux';
import { IStateWithNavigationPanel } from '../../../_State/NavigationPanel/model';

const DnaSimpleBarChartBody = styled.div<any>`
  height: 100%;
  width: 100%;
  overflow: hidden;
  .echarts-for-react {
    width: ${({ isOpen }) => (isOpen ? 'calc(100% - 20px)' : '100%')};
    height: 100% !important;
  }
`;

interface IProps {
  config: any;
}

export const DnaSimpleBarChart: FC<IProps> = ({ config }) => {
  const isOpen = useSelector<IStateWithNavigationPanel, boolean>(
    (state) => state.navigationPanel.isOpen
  );

  return (
    <DnaSimpleBarChartBody isOpen={isOpen}>
      <ReactEcharts className={'bar-chart'} option={config} />
    </DnaSimpleBarChartBody>
  );
};
