import React, { FC } from 'react';
import {
  Button,
  ButtonProps,
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core';
import { ICommonProps } from '_Types/props';
import styled from '@emotion/styled';

const StyledButton = styled(Button)<{ fullWidth?: boolean }>`
  padding: 1.1rem 3.2rem;
  text-transform: capitalize;
  position: relative;
  white-space: nowrap;
  width: ${({ fullWidth }) => (fullWidth ? '100%' : 'auto')};
`;

export enum ButtonVariant {
  Contained,
  Outlined,
  Text,
}
export enum ButtonType {
  Primary,
  Secondary,
}

export interface IProps extends ICommonProps {
  label?: string;
  isDisabled?: boolean;
  variant?: ButtonVariant;
  type?: ButtonType;
  inputType?: 'submit';
  size?: 'small' | 'medium' | 'large';
  onClick?: () => void;
  notificationQuantity?: number;
  fullWidth?: boolean;
  isProcessing?: boolean;
  color?: 'primary' | 'secondary' | 'default';
}

type MaterialPropsMapping = { [enumKey: number]: () => Partial<ButtonProps> };

const variantToMaterialProps: MaterialPropsMapping = {
  [ButtonVariant.Contained]: () => ({ variant: 'contained' }),
  [ButtonVariant.Outlined]: () => ({ variant: 'outlined' }),
  [ButtonVariant.Text]: () => ({ variant: 'text' }),
};

const typeToMaterialProps: MaterialPropsMapping = {
  [ButtonType.Primary]: () => ({ color: 'primary' }),
  [ButtonType.Secondary]: () => ({ color: 'secondary' }),
};

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    sizeSmall: {
      padding: '0.4rem 1.2rem',
      fontWeight: 200,
    },
  })
);

export const DnaButton: FC<IProps> = ({
  label,
  variant = ButtonVariant.Contained,
  inputType,
  type = ButtonType.Primary,
  onClick,
  className,
  size = 'medium',
  isDisabled,
  fullWidth,
  isProcessing,
  color,
}) => {
  const variantProps = variantToMaterialProps[variant]();
  const typeProps = typeToMaterialProps[type]();
  const classes = useStyles();
  return (
    <>
      <StyledButton
        color={color}
        fullWidth={fullWidth}
        classes={{ sizeSmall: classes.sizeSmall }}
        className={className}
        type={inputType}
        {...variantProps}
        {...typeProps}
        onClick={onClick}
        size={size}
        disabled={isDisabled || isProcessing}
      >
        {label}
      </StyledButton>
    </>
  );
};
