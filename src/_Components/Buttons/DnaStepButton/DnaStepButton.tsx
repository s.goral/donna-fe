import React, { FC } from 'react';
import styled from '@emotion/styled';
import { IThemed } from '_Styles/types';
import { ICommonProps } from '_Types/props';
import {
  DnaLinkWrapper,
  IProps as IDnaLinkWrapperProps,
} from '_Components/Buttons/DnaLinkWrapper/DnaLinkWrapper';

// TODO: remove important
const DnaStepButtonBody = styled(DnaLinkWrapper)<
  IDnaLinkWrapperProps & IThemed
>`
  display: inline-flex;
  color: ${({ theme }: IThemed) => theme.palette.primary.main} !important;
`;

const Text = styled.span<IThemed>`
  margin-left: ${({ theme }) => theme.spacing(1)};
  display: inline-flex;
`;

interface IProps extends ICommonProps {
  linkTo: string;
  text?: string;
}

export const DnaStepButton: FC<IProps> = ({ linkTo, text, className }) => {
  return (
    <DnaStepButtonBody to={linkTo} className={className}>
      <Text>{text}</Text>
    </DnaStepButtonBody>
  );
};
