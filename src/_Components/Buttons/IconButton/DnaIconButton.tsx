import { Button } from '@material-ui/core';
import React, { FC } from 'react';

import { Label, useButtonStyles, Wrapper } from './styles';
import { ICommonProps } from '_Types/props';
import { ButtonVariant } from '_Components/Buttons/DnaButton';

interface IProps extends ICommonProps {
  disabled?: boolean;
  isButton?: boolean;
  label?: string;
  iconSize?: number;
  buttonVariant?: ButtonVariant;
  onClick?: () => void;
}

export const DnaIconButton: FC<IProps> = ({
  disabled,
  isButton,
  label,
  iconSize = 15,
  buttonVariant = ButtonVariant.Text,
  onClick,
  children,
}) => {
  const styleProps = { iconSize, disabled, isButton, buttonVariant };
  const buttonClasses = useButtonStyles(styleProps);

  const handleClick = () => {
    if (!disabled) {
      onClick();
    }
  };

  return (
    <Wrapper isButton={isButton} onClick={handleClick} disabled={disabled}>
      <Button
        variant={'text'}
        disabled={disabled}
        color={disabled ? 'primary' : 'secondary'}
        classes={buttonClasses}
        disableElevation
      >
        {children}
      </Button>
      {label && <Label disabled={disabled}>{label}</Label>}
    </Wrapper>
  );
};
