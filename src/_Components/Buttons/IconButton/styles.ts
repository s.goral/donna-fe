import styled from '@emotion/styled';
import {
  ButtonProps,
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core';
import { themeGetters } from '_Styles/themeHelpers';
import { ButtonVariant } from '_Components/Buttons/DnaButton';
import { IThemed } from '_Styles/types';

const { color, spacing } = themeGetters;

interface ITooltipStyleProps {
  disabled: boolean;
}

export const useTooltipStyles = makeStyles((theme: Theme) => ({
  arrow: ({ disabled }: ITooltipStyleProps) => ({
    color: disabled ? theme.palette.grey[200] : theme.palette.secondary.light,
  }),
  tooltip: ({ disabled }: ITooltipStyleProps) => ({
    backgroundColor: disabled
      ? theme.palette.grey[200]
      : theme.palette.secondary.light,
    color: disabled ? theme.palette.primary.dark : theme.palette.secondary.dark,
    fontSize: 11,
    textAlign: 'center',
    fontWeight: 'normal',
  }),
}));

interface IButtonStyleProps {
  iconSize: number;
  disabled: boolean;
  isButton: boolean;
  buttonVariant: ButtonVariant;
}

export const useButtonStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: ({
      iconSize,
      isButton,
      disabled,
      buttonVariant,
    }: IButtonStyleProps) => ({
      padding: isButton ? theme.spacing(0, 1) : 0,
      margin: 0,
      fontSize: iconSize,
      minWidth: theme.spacing(3.4),
      '&:hover, &:active': {
        boxShadow: 'none',
        background:
          isButton && buttonVariant === ButtonVariant.Contained
            ? `${theme.palette.secondary.dark}`
            : disabled
            ? `${theme.palette.primary.main}15`
            : 'transparent',
      },
    }),
  })
);

interface IStyleProps extends IThemed {
  disabled: boolean;
  isButton?: boolean;
}

export const Wrapper = styled.div<IStyleProps>`
  display: flex;
  align-items: center;
  margin-right: ${({ isButton }) => (isButton ? spacing(1) : spacing(2))};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
`;

interface ILabelProps extends IThemed {
  disabled: boolean;
}

export const Label = styled.div<ILabelProps>`
  font-size: 15px;
  font-weight: 500;
  color: ${({ disabled }) =>
    disabled ? color('primary') : color('secondary')};
  padding-left: ${spacing(1)};
`;

type MaterialPropsMapping = { [enumKey: number]: () => Partial<ButtonProps> };

export const variantToMaterialProps: MaterialPropsMapping = {
  [ButtonVariant.Contained]: () => ({ variant: 'contained' }),
  [ButtonVariant.Outlined]: () => ({ variant: 'outlined' }),
};
