import styled from '@emotion/styled';
import { Divider } from '@material-ui/core';
import { IThemed } from '_Styles/types';

export const HorizontalDivider = styled(Divider)<IThemed>`
  && {
    margin: ${({ theme }) => theme.spacing(5, 0)};
  }
`;
