import React, { FC } from 'react';
import { ICommonProps } from '_Types/props';
import styled from '@emotion/styled';
import { ITile } from '_Types/ITile';
import { IThemed } from '_Styles/types';
import { DnaTile } from '_Components/DnaTilesPanel/DnaTile';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const Tiles = styled.div<IThemed>`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  width: 100%;
  height: 100%;
`;

interface IProps extends ICommonProps {
  tiles: ITile[];
  className?: string;
}

export const DnaTilesPanel: FC<IProps> = ({ tiles, className }) => {
  return (
    <Wrapper className={className}>
      <Tiles>
        {tiles.map(({ label, amount, component }, index) => (
          <DnaTile
            tilesQuantity={tiles.length}
            key={index}
            label={label}
            amount={amount}
            component={component}
          />
        ))}
      </Tiles>
    </Wrapper>
  );
};
