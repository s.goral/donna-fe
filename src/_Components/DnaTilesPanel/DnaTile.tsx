import React, { FC } from 'react';
import styled from '@emotion/styled';
import { ITile } from '_Types/ITile';
import { IThemed } from '_Styles/types';
import { isNil } from 'lodash';

const Wrapper = styled.div<IThemed & { tilesQuantity: number }>`
  margin: ${({ theme }) => theme.spacing(0, 3, 0, 0)};
  border-radius: 5px;
  box-shadow: 0px 4px 16px rgba(97, 91, 136, 0.15);
  background-color: ${({ theme }) => theme.palette.primary.light};
  height: 100%;
  flex-grow: 1;
  flex-shrink: 1;
  display: flex;
  justify-content: flex-start;
  width: ${({ tilesQuantity }) => `${100 / tilesQuantity}%`};
  &:last-of-type {
    margin: 0;
  }
`;

const Label = styled.div`
  color: #767676;
  font-size: 1.4rem;
  font-family: Rubik, sans-serif;
`;

const Amount = styled.div<IThemed & { editable?: boolean }>`
  margin-top: ${({ theme }) => theme.spacing(0.25)};
  color: ${({ theme }) => theme.palette.primary.contrastText};
  font-weight: bold;
  font-size: 3.4rem;
  margin-top:1rem
  :hover {
    background-color: ${({ editable }) => (editable ? '#F4F4F4' : 'white')};
    transition: background-color 0.2s;
    cursor: pointer;
  }
`;

const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

interface IProps extends ITile {
  tilesQuantity: number;
  onChange?: (tileName: string, value: any) => void;
  className?: string;
}
export const DnaTile: FC<IProps> = ({
  label,
  amount,
  tilesQuantity,
  component,
  className,
}) => {
  return (
    <Wrapper tilesQuantity={tilesQuantity} className={className}>
      <TextWrapper>
        {label && <Label>{label}</Label>}
        {!isNil(amount) && <Amount>{amount}</Amount>}
        {component && component}
      </TextWrapper>
    </Wrapper>
  );
};
