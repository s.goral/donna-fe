import React, { ChangeEvent, FC, FocusEvent } from 'react';
import { InputAdornment, TextField } from '@material-ui/core';
import styled from '@emotion/styled';
import { IThemed } from '_Styles/types';
import { ICommonProps } from '_Types/props';

const Input = styled(TextField)<IThemed>`
  background-color: ${({ theme }) => theme.palette.background.paper};
`;

const Wrapper = styled.div`
  position: relative;
`;

export interface IInputProps extends ICommonProps {
  value?: unknown;
  defaultValue?: string;
  variant?: 'standard' | 'outlined' | 'filled';
  placeholder?: string;
  label?: string;
  typeNumber?: boolean;
  type?: string;
  name?: string;
  required?: boolean;
  disabled?: boolean;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  endAdornment?: any;
  multiline?: boolean;
  rows?: string | number;
  rowsMax?: string | number;
  fullWidth?: boolean;
  margin?: 'dense' | 'none';
  onUpArrowClick?: () => void;
  onDownArrowClick?: () => void;
  onFocus?: () => void;
  onBlur?: (event: FocusEvent<HTMLInputElement>) => void;
  error?: boolean;
  mask?: boolean;
}
export const DnaInput: FC<IInputProps> = ({
  value,
  defaultValue,
  placeholder,
  label,
  className,
  name,
  required = false,
  onChange,
  disabled,
  endAdornment,
  multiline,
  error,
  rows,
  rowsMax,
  fullWidth = true,
  margin,
  type,
  onFocus,
  onBlur,
  mask,
}) => {
  return (
    <Wrapper className={className}>
      <Input
        margin={margin}
        disabled={disabled}
        value={value}
        className={className}
        placeholder={placeholder}
        label={label}
        type={type}
        variant={'outlined'}
        defaultValue={defaultValue}
        name={name}
        error={error}
        required={required}
        onChange={onChange}
        fullWidth={fullWidth}
        multiline={multiline}
        rows={rows}
        rowsMax={rowsMax}
        onFocus={onFocus}
        onBlur={onBlur}
        InputLabelProps={{
          shrink: true,
        }}
        InputProps={{
          endAdornment: endAdornment && (
            <InputAdornment position="end">{endAdornment}</InputAdornment>
          ),
        }}
      />
    </Wrapper>
  );
};
