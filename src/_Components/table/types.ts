import { ReactElement } from 'react';
import { SerializedStyles } from '@emotion/serialize';

export interface ICellConfig<RowData> {
  label: string;
  propKey?: string;
  key?: string;
  cellRender?: (rowData: RowData) => ReactElement;
  headerCompactCellRender?: ReactElement;
  className?: string;
  css?: SerializedStyles;
}

export interface IRowConditionalRender<RowData> {
  condition: (rowData: RowData) => boolean;
  render: (rowData: RowData) => ReactElement;
}
