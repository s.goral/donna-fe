import isPropValid from '@emotion/is-prop-valid';
import styled from '@emotion/styled';
import TableCell from '@material-ui/core/TableCell';
import React from 'react';

import { ICellConfig } from './types';
import { IThemed } from '_Styles/types';
import { themeGetters } from '_Styles/themeHelpers';

const { spacing } = themeGetters;

interface ITextProps extends IThemed {
  isLastCell?: boolean;
  isFirstCell?: boolean;
}

const TableText = styled.span<ITextProps>`
  margin-right: ${({ isLastCell }) => isLastCell && spacing(5)};
  margin-left: ${({ isFirstCell }) => isFirstCell && spacing(5)};
`;
const HeaderText = styled.span<ITextProps>`
  margin-right: ${({ isLastCell }) => isLastCell && spacing(5)};
  margin-left: ${({ isFirstCell }) => isFirstCell && spacing(5)};
`;

interface ICellProps extends IThemed {
  isCompact?: boolean;
}

const StyledCell = styled(TableCell, {
  shouldForwardProp: (prop) => isPropValid(prop),
})<ICellProps>`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  background-color: white !important;
  width: ${({ isCompact }) => isCompact && spacing(2.8)};
`;

const HeaderCell = styled(TableCell, {
  shouldForwardProp: (prop) => isPropValid(prop),
})<ICellProps>`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  background-color: #e2d7d5 !important;
  width: ${({ isCompact }) => isCompact && spacing(2.8)};
`;

export const prepareCreateRowCells = <T extends {}>(
  configs: ICellConfig<T>[]
) => (rowData: T) => {
  return configs.map(({ key, propKey, cellRender, className }, index) => {
    const reactKey = key || propKey;
    if (cellRender) {
      return (
        <StyledCell key={reactKey} className={className}>
          {cellRender(rowData)}
        </StyledCell>
      );
    } else if (propKey) {
      const value = rowData[propKey];
      const isFirstCell = index === 0;
      const isLastCell = index === configs.length - 1;

      return (
        <StyledCell key={reactKey} className={className}>
          <TableText isFirstCell={isFirstCell} isLastCell={isLastCell}>
            {value}
          </TableText>
        </StyledCell>
      );
    }

    console.warn('DnaTable.createRowCells missing one of: propKey, cellRender');
    return null;
  });
};

export const createHeaderCells = <T extends {}>(configs: ICellConfig<T>[]) => {
  return configs.map(
    ({ key, propKey, label, className, headerCompactCellRender }, index) => {
      const reactKey = key || propKey;
      const isFirstCell = index === 0;
      const isLastCell = index === configs.length - 1;

      if (headerCompactCellRender) {
        return (
          <HeaderCell key={reactKey} className={className} isCompact>
            {headerCompactCellRender}
          </HeaderCell>
        );
      } else {
        return (
          <HeaderCell key={reactKey} className={className}>
            <HeaderText
              key={propKey}
              isFirstCell={isFirstCell}
              isLastCell={isLastCell}
            >
              {label.toUpperCase()}
            </HeaderText>
          </HeaderCell>
        );
      }
    }
  );
};
