import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const tableStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      flex: '1 1 auto',
      height: 0,
      display: 'flex',
    },
    table: {
      overflowY: 'auto',
      overflowX: 'hidden',
    },
  })
);
