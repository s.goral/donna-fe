import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import React, { useMemo } from 'react';
import { ICellConfig, IRowConditionalRender } from './types';
import { createHeaderCells, prepareCreateRowCells } from './cell';
import { tableStyles } from './styles';
import { ICommonProps } from '_Types/props';

interface IProps<RowData extends {}> extends ICommonProps {
  configs: ICellConfig<RowData>[];
  data: RowData[];
  rowKey: string;
  rowConditionalRender?: IRowConditionalRender<RowData>;
}

export const DnaTable = <T extends {}>({
  className,
  configs,
  data,
  rowKey,
  rowConditionalRender,
}: IProps<T>) => {
  const classes = tableStyles();
  const createRowCells = useMemo(() => prepareCreateRowCells(configs), [
    configs,
  ]);

  const Rows = useMemo(
    () =>
      data.map((rowData) => {
        if (rowConditionalRender && rowConditionalRender.condition(rowData)) {
          return rowConditionalRender.render(rowData);
        }

        const key = rowData[rowKey];

        return <TableRow key={key}>{createRowCells(rowData)}</TableRow>;
      }),
    [data, rowConditionalRender, rowKey, createRowCells]
  );

  const Header = useMemo(() => createHeaderCells(configs), [configs]);

  return (
    <TableContainer className={classes.paper} component={Paper}>
      <Table className={className} stickyHeader={true}>
        <TableHead>
          <TableRow>{Header}</TableRow>
        </TableHead>

        <TableBody>{Rows}</TableBody>
      </Table>
    </TableContainer>
  );
};
