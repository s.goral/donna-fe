import React, { ChangeEvent, FC } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { css } from 'emotion';
import { ICommonProps } from '../_Types/props';

interface IProps extends ICommonProps {
  label: string;
  options: any[];
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  value: any;
  width?: string;
  placeholder?: string;
}

export const DnaSelect: FC<IProps> = ({
  label,
  options,
  onChange,
  value,
  width = '30%',
  placeholder,
  className,
}) => {
  return (
    <FormControl className={className} variant="outlined">
      <InputLabel shrink id="demo-simple-select-outlined-label">
        {label}
      </InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        value={value}
        onChange={onChange}
        label={label}
        placeholder={placeholder}
      >
        {options.map(({ value, label, id }) => (
          <MenuItem key={id} value={value}>
            {label}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
