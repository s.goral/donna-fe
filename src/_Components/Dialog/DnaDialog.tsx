import React, { FC, ReactElement, ReactNode } from 'react';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import styled from '@emotion/styled';
import { themeGetters } from '_Styles/themeHelpers';
import { DialogType } from '_Components/Dialog/types';
import { ICommonProps } from '_Types/props';
import { DnaDialogTitle } from './DnaDialogTitle';
import { CheckCircle } from '@material-ui/icons';
import ErrorIcon from '@material-ui/icons/Error';
import { IThemed } from '_Styles/types';

interface IStyleProps extends IThemed {
  width?: number;
  height?: number;
  type?: DialogType;
}

const { spacing } = themeGetters;

const InfoText = styled.div`
  font-size: ${spacing(1.4)};
  font-weight: normal;
  padding-top: ${spacing(0.5)};
`;

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const Wrapper = styled.div<IStyleProps>`
  display: flex;
  flex-direction: row;
`;

const TextWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  text-align: center;
  width: 100%;
  margin-bottom: 3rem;
`;

const dialogStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: ({ width, height }: IStyleProps) => ({
      borderRadius: 0,
      height: height ? `${height}rem` : 'default',
      width: width ? `${width}rem` : 'default',
      maxWidth: '80rem',
    }),
  })
);

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(1.5),
    overflow: 'hidden',
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(0.5, 6, 2.5, 7),
    justifyContent: 'center',
  },
}))(MuiDialogActions);

const IconWrapper = styled.div<IStyleProps>`
  .MuiSvgIcon-root {
    font-size: 4rem !important;
    color: ${({ theme, type }) =>
      type === DialogType.SUCCESS
        ? theme.palette.success.main
        : theme.palette.error.main};
  }
`;

interface IProps extends ICommonProps {
  isOpen?: boolean;
  type?: DialogType;
  title?: string | ReactElement;
  infoText?: string | ReactElement;
  textAlign?: string;
  width?: number;
  height?: number;
  actions?: ReactNode | ReactNode[];
  onOpen?: () => void;
  onClose?: () => void;
  overrideClasses?: ClassNameMap;
}

export const DnaDialog: FC<IProps> = ({
  isOpen = false,
  type = DialogType.CONFIRMATION,
  title,
  infoText,
  width = 50,
  height = 30,
  actions,
  onOpen,
  onClose,
  children,
  overrideClasses,
}) => {
  const styleProps = { type, width, height };
  const classes = dialogStyles(styleProps);

  const transitionDuration = 500;

  const DialogClasses = {
    paper: classes.paper,
    ...overrideClasses,
  };

  const renderIcon = () => {
    return type === DialogType.SUCCESS ? (
      <CheckCircle />
    ) : type === DialogType.ERROR ? (
      <ErrorIcon />
    ) : null;
  };

  return (
    <Dialog
      transitionDuration={transitionDuration}
      classes={DialogClasses}
      onClose={onClose}
      onEnter={onOpen}
      aria-labelledby="dialog-title"
      open={isOpen}
    >
      <Wrapper>
        <DnaDialogTitle id="dialog-title" type={type} onClose={onClose}>
          <TitleWrapper>
            <TextWrapper>
              <IconWrapper type={type}>{renderIcon()}</IconWrapper>
              {title && title}
              {infoText && <InfoText>{infoText}</InfoText>}
            </TextWrapper>
          </TitleWrapper>
        </DnaDialogTitle>
      </Wrapper>
      {children && <DialogContent>{children}</DialogContent>}
      {actions && <DialogActions>{actions}</DialogActions>}
    </Dialog>
  );
};
