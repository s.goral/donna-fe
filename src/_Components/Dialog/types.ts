export enum DialogType {
  SUCCESS = 'SUCCESS',
  CONFIRMATION = 'CONFIRMATION',
  ERROR = 'ERROR',
}
