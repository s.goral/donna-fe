import React, { FC } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MuiDialogTitle, {
  DialogTitleProps,
} from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { DialogType } from '_Components/Dialog/types';
import { ICommonProps } from '_Types/props';
import { Close } from '@material-ui/icons';

interface IStyleProps {
  type: DialogType;
}

const styles = makeStyles((theme: Theme) =>
  createStyles({
    root: ({ type }: IStyleProps) => ({
      color:
        type === DialogType.SUCCESS
          ? theme.palette.success.main
          : type === DialogType.ERROR
          ? theme.palette.error.main
          : theme.palette.primary.dark,
      fontSize: theme.spacing(1.8),
      fontWeight: 500,
      textAlign: 'center',
      display: 'flex',
      flexDirection: 'row',
      padding: theme.spacing(2.5, 4, 0, 4),
      width: '100%',
    }),
    closeButton: {
      position: 'absolute',
      padding: 0,
      top: theme.spacing(0.5),
      right: theme.spacing(0.5),
      height: theme.spacing(3.5),
      width: theme.spacing(3.5),
      color: theme.palette.primary.main,
    },
  })
);
type IProps = DialogTitleProps &
  ICommonProps & {
    id: string;
    type: DialogType;
    onClose: () => void;
  };

export const DnaDialogTitle: FC<IProps> = ({ type, ...props }) => {
  const { children, onClose, ...other } = props;
  const styleProps = { type };

  const classes = styles(styleProps);

  const CloseButton = (
    <IconButton className={classes.closeButton} onClick={onClose}>
      <Close />
    </IconButton>
  );

  return (
    <MuiDialogTitle
      disableTypography={true}
      className={classes.root}
      {...other}
    >
      {children}
      <div>{onClose && CloseButton}</div>
    </MuiDialogTitle>
  );
};
