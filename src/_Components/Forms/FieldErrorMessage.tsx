import React, { FC } from 'react';
import styled from '@emotion/styled';
import { ICommonProps } from '_Types/props';
import { IThemed } from '_Styles/types';
import { Error } from '@material-ui/icons';
import { css } from 'emotion';

const ErrorMessageBody = styled.div<IThemed>`
  display: flex;
  align-items: center;
  height: 5rem;
  padding: 1rem 0;
  color: ${({ theme }) => theme.palette.error.main};
`;

const Message = styled.div<IThemed>`
  margin-left: 1rem;
  line-height: 2rem;
`;

interface IProps extends ICommonProps {
  message?: string | string[] | (string | undefined)[];
}

export const FieldErrorMessage: FC<IProps> = ({ message, className }) => {
  return (
    <ErrorMessageBody className={className}>
      {((Array.isArray(message) && message.some((el) => el !== undefined)) ||
        (!Array.isArray(message) && message)) && (
        <Error
          className={css`
            color: #fa4e61;
          `}
        />
      )}
      <Message>{message}</Message>
    </ErrorMessageBody>
  );
};
