import React, { FC } from 'react';
import { ICommonProps } from '_Types/props';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { FieldErrorMessage } from '_Components/Forms/FieldErrorMessage';

const RequiredPointer = styled.span<IThemed>`
  color: ${({ theme }) => theme.palette.error.main};
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.div<IThemed>`
  display: flex;
  flex-direction: row;
  color: ${({ theme }) => theme.palette.primary.contrastText};
  font-size: 2rem;
  margin: ${({ theme }) => theme.spacing(2, 0)};
`;

interface IProps extends ICommonProps {
  title?: string;
  required?: boolean;
  error?: string;
}

export const FormFieldWrapper: FC<IProps> = ({
  required = false,
  title,
  children,
  className,
  error,
}) => {
  return (
    <Wrapper className={className}>
      {title && (
        <Title>
          {title}
          {required && <RequiredPointer>*</RequiredPointer>}
        </Title>
      )}
      <div>{children}</div>
      <FieldErrorMessage message={error} />
    </Wrapper>
  );
};
