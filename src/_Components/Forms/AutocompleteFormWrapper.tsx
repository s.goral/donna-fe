import React, { FC } from 'react';
import { ICommonProps } from '_Types/props';
import { FormFieldWrapper } from '_Components/Forms/FormFieldWrapper';
import { DnaAutocomplete } from '../DnaAutocomplete';

interface IProps extends ICommonProps {
  fieldName?: string;
  title?: string;
  required?: boolean;
  error?: any;
  data: any[];
  label?: string;
  onChange?: (value: any) => void;
  value?: any;
}
export const AutocompleteFormWrapper: FC<IProps> = ({
  fieldName,
  className,
  title,
  required,
  error,
  data,
  label,
  onChange,
  value,
}) => {
  return (
    <FormFieldWrapper
      className={className}
      title={title}
      required={required}
      error={error}
    >
      <DnaAutocomplete
        value={value}
        label={label}
        data={data}
        onChange={onChange}
      />
    </FormFieldWrapper>
  );
};
