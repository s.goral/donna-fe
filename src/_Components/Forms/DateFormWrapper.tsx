import React, { FC } from 'react';
import { DnaLocale } from '_Components/Pickers/date-picker.models';
import { ICommonProps } from '_Types/props';
import { FormFieldWrapper } from '_Components/Forms/FormFieldWrapper';
import { DnaDatePicker } from '_Components/Pickers/DnaDatePicker/DnaDatePicker';

interface IProps extends ICommonProps {
  title: string;
  fieldName: string;
  locale?: DnaLocale;
  value?: any;
  disabled?: boolean;
  error?: string;
  required?: boolean;
  onChange?: (value: string) => void;
  onSave: (name: string, value: string) => void;
}

export const DateFormWrapper: FC<IProps> = ({
  title,
  fieldName,
  value,
  locale = DnaLocale.EN,
  disabled = false,
  required = false,
  error,
  onSave,
}) => {
  const handleChange = (selectedValue: string) => {
    onSave(fieldName, selectedValue);
  };

  return (
    <FormFieldWrapper required={required} title={title} error={error}>
      <DnaDatePicker
        value={value}
        locale={locale}
        onChange={handleChange}
        disabled={disabled}
        isError={!!error}
      />
    </FormFieldWrapper>
  );
};
