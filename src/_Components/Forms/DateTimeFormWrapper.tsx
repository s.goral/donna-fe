import React, { FC } from 'react';
import { DnaLocale } from '_Components/Pickers/date-picker.models';
import { ICommonProps } from '_Types/props';
import { FormFieldWrapper } from '_Components/Forms/FormFieldWrapper';
import { DnaDateTimePicker } from '../Pickers/DnaDateTimePicker/DnaDateTimePicker';

interface IProps extends ICommonProps {
  title?: string;
  fieldName: string;
  locale?: DnaLocale;
  value?: any;
  disabled?: boolean;
  error?: any;
  required?: boolean;
  onChange?: (value: string) => void;
  onSave: (name: string, value: string) => void;
  label?: string;
  minDate?: string;
}

export const DateTimeFormWrapper: FC<IProps> = ({
  className,
  title,
  fieldName,
  value,
  locale = DnaLocale.EN,
  disabled = false,
  required = false,
  onChange,
  error,
  onSave,
  label,
  minDate,
}) => {
  const handleChange = (selectedValue: string) => {
    onChange(selectedValue);
    onSave(fieldName, selectedValue);
  };

  return (
    <FormFieldWrapper
      className={className}
      required={required}
      title={title}
      error={error}
    >
      <DnaDateTimePicker
        minDate={minDate}
        label={label}
        value={value}
        locale={locale}
        onChange={handleChange}
        disabled={disabled}
        isError={!!error}
      />
    </FormFieldWrapper>
  );
};
