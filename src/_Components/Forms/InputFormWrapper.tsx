import React, {
  ChangeEvent,
  FC,
  ReactNode,
  FocusEvent,
  useState,
  useEffect,
} from 'react';
import { ICommonProps } from '_Types/props';
import { FormFieldWrapper } from '_Components/Forms/FormFieldWrapper';
import { DnaInput } from '_Components/DnaInput';

interface IProps extends ICommonProps {
  placeholder?: string;
  fullWidth?: boolean;
  label?: string;
  title?: string;
  tooltip?: string;
  value?: string;
  fieldName: string;
  required?: boolean;
  error?: string;
  rows?: number;
  type?: string;
  typeNumber?: boolean;
  setFieldValue?: (field: string, value: any, shouldValidate?: boolean) => void;
  rowsMax?: number;
  multiline?: boolean;
  endAdornment?: ReactNode;
  onBlur?: (event: FocusEvent<HTMLInputElement>) => void;
  handleChange?: (
    eventOrPath: string | React.ChangeEvent<any>
  ) => void | ((eventOrTextValue: string | React.ChangeEvent<any>) => void);
  mask?: boolean;
  disabled?: boolean;
}
export const InputFormWrapper: FC<IProps> = ({
  value,
  tooltip,
  title,
  required,
  type,
  typeNumber,
  fieldName,
  placeholder,
  error,
  label,
  rows,
  rowsMax,
  multiline = false,
  endAdornment,
  fullWidth = true,
  handleChange,
  onBlur,
  className,
  setFieldValue,
  mask,
  disabled,
}) => {
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    if (value) {
      setInputValue(value);
    }
  }, [value]);

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
    if (handleChange) {
      handleChange(event);
    }
  };

  return (
    <FormFieldWrapper
      className={className}
      title={title}
      required={required}
      error={error}
    >
      <DnaInput
        value={inputValue}
        disabled={disabled}
        fullWidth={fullWidth}
        multiline={multiline}
        type={type}
        typeNumber={typeNumber}
        rows={rows}
        rowsMax={rowsMax}
        name={fieldName}
        onChange={onChange}
        onBlur={onBlur}
        label={label}
        error={!!error}
        placeholder={placeholder}
        endAdornment={endAdornment}
        mask={mask}
      />
    </FormFieldWrapper>
  );
};
