import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { ICommonProps } from '_Types/props';
import { FormFieldWrapper } from '_Components/Forms/FormFieldWrapper';
import { DnaRadioGroup } from '_Components/Radio/DnaRadioGroup';

interface IProps extends ICommonProps {
  title: string;
  fieldName: string;
  value?: any;
  disabled?: boolean;
  required?: boolean;
  error?: string;
  onChange?: (value: string) => void;
  setFieldValue?: (field: string, value: any, shouldValidate?: boolean) => void;
  options: {
    value: string;
    label: string;
  }[];
}

export const RadioGroupFormWrapper: FC<IProps> = ({
  title,
  fieldName,
  value,
  error,
  disabled = false,
  required = false,
  setFieldValue,
  options,
}) => {
  const [radioValue, setRadioValue] = useState('');

  useEffect(() => {
    if (value) {
      setRadioValue(value);
    }
  }, [value]);

  const handleChange = (
    event: ChangeEvent<HTMLInputElement>,
    value: string
  ) => {
    setRadioValue(value);
    setFieldValue(fieldName, value);
  };

  return (
    <FormFieldWrapper required={required} title={title} error={error}>
      <DnaRadioGroup
        value={radioValue}
        onChange={handleChange}
        options={options}
      />
    </FormFieldWrapper>
  );
};
