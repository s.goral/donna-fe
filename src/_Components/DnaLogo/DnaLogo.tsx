import React, { FC } from 'react';
import styled from '@emotion/styled';
import Logo from './Logo.svg';

const StyledLogo = styled.div`
  height: 100%;
  width: 100%;
  background: url(${Logo}) center no-repeat;
`;

export const DnaLogo: FC = () => {
  return <StyledLogo />;
};
