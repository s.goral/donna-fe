import React, { FC } from 'react';
import styled from '@emotion/styled';
import SmallLogo from './SmallLogo.svg';

const StyledLogo = styled.div`
  height: 100%;
  width: 100%;
  background: url(${SmallLogo}) center no-repeat;
`;

export const DnaSmallLogo: FC = () => {
  return <StyledLogo />;
};
