import enLocale from 'date-fns/locale/en-US';
import deLocale from 'date-fns/locale/de';
import DateFnsUtils from '@date-io/date-fns';
import { format, Locale } from 'date-fns';

export enum DnaLocale {
  EN = 'en',
}

interface ILocaleMap {
  [key: string]: Locale;
}

interface ILocaleUtils {
  [key: string]: typeof DateFnsUtils;
}

interface ISingleTranslation {
  [key: string]: string;
}

type WeekDaysNumbers = 0 | 1 | 2 | 3 | 4 | 5 | 6;
const dnaEnLocale = {
  ...enLocale,
  options: {
    ...enLocale.options,
    weekStartsOn: 1 as WeekDaysNumbers,
  },
};

const dnaDeLocale = {
  ...deLocale,
  options: {
    ...deLocale.options,
    weekStartsOn: 1 as WeekDaysNumbers,
  },
};

export const localeMap: ILocaleMap = {
  en: dnaEnLocale,
  de: dnaDeLocale,
};

export const localeCancelLabelMap: ISingleTranslation = {
  en: 'cancel',
  de: 'abbrechen',
};

export const localeOkLabelMap: ISingleTranslation = {
  en: 'ok',
  de: 'ok',
};

export class DeLocalizedUtils extends DateFnsUtils {
  getDatePickerHeaderText(date: Date) {
    return format(date, 'd MMM yyyy', { locale: this.locale });
  }
}

export const localeUtilsMap: ILocaleUtils = {
  en: DateFnsUtils,
  de: DeLocalizedUtils,
};
