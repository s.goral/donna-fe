import React, { FC, useState, useEffect } from 'react';
import { format, isValid } from 'date-fns';
import { MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { TextField } from '@material-ui/core';

import {
  localeMap,
  localeUtilsMap,
  localeCancelLabelMap,
  localeOkLabelMap,
  DnaLocale,
} from '../date-picker.models';
import { ICommonProps } from '_Types/props';

interface IProps extends ICommonProps {
  value?: string | undefined;
  disabled?: boolean;
  isError?: boolean;
  locale?: DnaLocale;
  required?: boolean;
  onChange: (value: string) => void;
  label?: string;
  minDate?: string;
}

export const DnaDateTimePicker: FC<IProps> = ({
  value = null,
  isError = false,
  disabled = false,
  locale = DnaLocale.EN,
  label,
  onChange,
  minDate,
}) => {
  const [selectedDate, setSelectedDate] = useState(value || new Date());

  useEffect(() => {
    setSelectedDate(value);
  }, [value]);

  const handleDateChange = (date: MaterialUiPickersDate) => {
    if (isValid(date)) {
      const formattedDate = format(date, 'yyyy-MM-dd HH:mm');
      setSelectedDate(date);
      onChange(formattedDate);
    }
  };

  return (
    <MuiPickersUtilsProvider
      utils={localeUtilsMap[locale]}
      locale={localeMap[locale]}
    >
      <DateTimePicker
        minDate={minDate}
        minutesStep={15}
        ampm={false}
        value={selectedDate}
        minDateMessage={null}
        maxDateMessage={null}
        cancelLabel={localeCancelLabelMap[locale]}
        okLabel={localeOkLabelMap[locale]}
        onChange={handleDateChange}
        disablePast
        disabled={disabled}
        format="dd/MM/yyyy HH:mm"
        inputVariant="outlined"
        TextFieldComponent={(params) => (
          <TextField
            {...params}
            label={label}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
          />
        )}
      />
    </MuiPickersUtilsProvider>
  );
};
