import React, { FC, useState, useEffect } from 'react';
import { format, isValid } from 'date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { MuiThemeProvider } from '@material-ui/core';

import {
  localeMap,
  localeUtilsMap,
  localeCancelLabelMap,
  localeOkLabelMap,
  DnaLocale,
} from '../date-picker.models';
import { ICommonProps } from '_Types/props';
import {
  datePickerTheme,
  useDatePickerStyles,
} from '_Components/Pickers/styles';
import DateRangeIcon from '@material-ui/icons/DateRange';

interface IProps extends ICommonProps {
  value?: string | undefined;
  disabled?: boolean;
  isError?: boolean;
  locale?: DnaLocale;
  required?: boolean;
  onChange: (value: string) => void;
}

export const DnaDatePicker: FC<IProps> = ({
  value = null,
  isError = false,
  disabled = false,
  locale = DnaLocale.EN,
  onChange,
}) => {
  const [selectedDate, setSelectedDate] = useState(value || new Date());

  useEffect(() => {
    setSelectedDate(value);
  }, [value]);

  const props = { isError, disabled };
  const classes = useDatePickerStyles(props);
  const dateFormat = 'dd/MM/yyyy';

  const InputProps = {
    className: classes.input,
    endAdornment: <DateRangeIcon />,
  };

  const handleDateChange = (date: MaterialUiPickersDate) => {
    if (isValid(date)) {
      const formattedDate = format(date, 'yyyy-MM-dd');
      setSelectedDate(date);
      onChange(formattedDate);
    }
  };

  return (
    <MuiThemeProvider theme={datePickerTheme}>
      <MuiPickersUtilsProvider
        utils={localeUtilsMap[locale]}
        locale={localeMap[locale]}
      >
        <KeyboardDatePicker
          clearable
          disabled={disabled}
          value={selectedDate}
          format={dateFormat}
          minDateMessage={null}
          maxDateMessage={null}
          cancelLabel={localeCancelLabelMap[locale]}
          okLabel={localeOkLabelMap[locale]}
          onChange={handleDateChange}
          InputProps={InputProps}
        />
      </MuiPickersUtilsProvider>
    </MuiThemeProvider>
  );
};
