import {
  makeStyles,
  Theme,
  createStyles,
  createMuiTheme,
} from '@material-ui/core';
import { IThemed } from '_Styles/types';

interface IStyleProps extends IThemed {
  isError?: boolean;
  disabled?: boolean;
}

export const useDatePickerStyles = makeStyles((theme: Theme) =>
  createStyles({
    input: ({ isError, disabled }: IStyleProps) => ({
      height: theme.spacing(5.4),
      width: '100%',
      fontSize: '14px',
      paddingLeft: theme.spacing(1),
      border: `1px solid ${
        isError
          ? theme.palette.error.main
          : disabled
          ? theme.palette.grey[100]
          : theme.palette.grey[200]
      }`,
      color: theme.palette.primary.contrastText,
      borderRadius: '4px',
      backgroundColor: disabled
        ? theme.palette.grey[100]
        : theme.palette.background.paper,
    }),
  })
);

export const datePickerTheme = createMuiTheme({
  typography: {
    fontFamily: '"Rubik", "Roboto", sans-serif',
    button: {
      fontSize: '14px',
      fontWeight: 500,
    },
  },
  palette: {
    primary: {
      main: '#b2857c',
    },
  },
  overrides: {
    MuiTypography: {
      caption: {
        fontSize: 14,
      },
      body1: {
        fontSize: 16,
      },
      body2: {
        fontSize: 14,
      },
      subtitle1: {
        fontSize: 14,
      },
    },
    MuiFormControl: {
      root: {
        width: '100%',
        marginTop: '16px',
        marginBottom: '8px',
      },
    },
    MuiInputLabel: {
      outlined: {
        '&$shrink': {
          fontSize: '1.4rem',
          color: '#0F0F0F',
        },
      },
    },
    MuiFormLabel: {
      root: {
        fontSize: '1.4rem',
      },
    },
    MuiInput: {
      underline: {
        '&:before': {
          borderBottom: 'none',
          content: 'none',
        },
        '&:after': {
          borderBottom: 'none',
          content: 'none',
        },
      },
    },
  },
});
