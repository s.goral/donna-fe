import {
  CaseReducer,
  createSlice,
  Dispatch,
  PayloadAction,
} from '@reduxjs/toolkit';
import { getDetails, IUserLoginPayload, loginUser } from '_Api/auth';
import { setCredentials } from '_Api/api';
import { userActions } from '../User/state';
import { RoleName } from '../../_Types/AppTypes';

export interface IAuthState {
  token?: string;
}

const parseJwt = (token) => {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split('')
      .map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join('')
  );

  return JSON.parse(jsonPayload);
};

const initialState: IAuthState = {
  token: undefined,
};

type AuthReducer<PayloadType> = CaseReducer<
  IAuthState,
  PayloadAction<PayloadType>
>;

const saveCredentials: AuthReducer<string> = (state, { payload }) => {
  state.token = payload;
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    saveCredentials,
  },
});
export const authActions = authSlice.actions;
export const authReducer = authSlice.reducer;

export const getUserDetails = () => {
  return async (dispatch: Dispatch) => {
    try {
      const userDetails = await getDetails();
      dispatch(userActions.userLogin(userDetails));
    } catch (e) {}
  };
};

export const login = (
  loginPayload: IUserLoginPayload,
  history: any,
  onLoginFailure: () => void
) => {
  return async (dispatch: Dispatch) => {
    try {
      const { token } = await loginUser(loginPayload);
      dispatch(authActions.saveCredentials(token));
      setCredentials({ token });
      const userDetails = await getDetails();
      dispatch(userActions.userLogin(userDetails));
      sessionStorage.setItem('access_token', token);
      userDetails.roleName === RoleName.ROLE_ADMIN
        ? history.push('/admin/clients')
        : history.push('/dashboard');
    } catch (httpError) {
      onLoginFailure();
    }
  };
};
