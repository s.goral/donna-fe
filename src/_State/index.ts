import { IAuthState } from '_State/Auth/state';
import { IUserState } from '_State/User/state';
import { INavigationPanelState } from '_State/NavigationPanel/model';
import { IDashboardState } from '_State/Dashboard/model';

export interface IState {
  auth: IAuthState;
  user: IUserState;
  navigationPanel: INavigationPanelState;
  dashboard: IDashboardState;
}
