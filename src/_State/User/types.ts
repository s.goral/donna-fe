import { RoleName } from '../../_Types/AppTypes';

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  roleName: RoleName;
}
