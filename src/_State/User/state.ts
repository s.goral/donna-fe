import { CaseReducer, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IUser } from './types';
import { IState } from '../index';

export interface IUserState {
  user?: IUser;
}

const initialState: IUserState = {
  user: undefined,
};

type UserReducer<ActionPayload> = CaseReducer<
  IUserState,
  PayloadAction<ActionPayload>
>;

export const getUserState = (state: IState) => {
  return state.user;
};

const userLogin: UserReducer<IUser> = (state, { payload: user }) => {
  state.user = user;
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    userLogin,
  },
});

export const userActions = userSlice.actions;
export const userReducer = userSlice.reducer;
