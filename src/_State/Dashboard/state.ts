import {
  createSlice,
  CaseReducer,
  PayloadAction,
  createSelector,
} from '@reduxjs/toolkit';
import { IDashboardState, IStateWithDashboard } from '_State/Dashboard/model';
import { IClient, IStatistics } from '_Types/AppTypes';
import { identity, cloneDeep } from 'lodash';
import { getClients, getStatistics } from '_Api/company.api';

const setClients: CaseReducer<IDashboardState, PayloadAction<IClient[]>> = (
  state,
  { payload }
) => {
  state.clients = payload;
};

const setStatistics: CaseReducer<
  IDashboardState,
  PayloadAction<IStatistics>
> = (state, { payload }) => {
  state.statistics = payload;
};

const INITIAL_STATE: IDashboardState = {
  clients: [],
  statistics: undefined,
};

const dashboardSlice = createSlice({
  name: 'dashboard',
  initialState: INITIAL_STATE,
  reducers: {
    setClients,
    setStatistics,
    clearState: (state) => {
      Object.assign(state, cloneDeep(INITIAL_STATE));
    },
  },
});

export const getDashboardStatistics = createSelector<
  IStateWithDashboard,
  IStatistics,
  IStatistics
>((state) => state.dashboard.statistics, identity);

export const getDashboardClients = createSelector<
  IStateWithDashboard,
  IClient[],
  IClient[]
>((state) => state.dashboard.clients, identity);

export const getDashboardState = createSelector<
  IStateWithDashboard,
  IDashboardState,
  IDashboardState
>((state) => state.dashboard, identity);

export const loadDashboardData = () => {
  return async (dispatch) => {
    try {
      const clients = await getClients();
      const statistics = await getStatistics();
      dispatch(dashboardSlice.actions.setClients(clients));
      dispatch(dashboardSlice.actions.setStatistics(statistics));
    } finally {
    }
  };
};

export const dashboardReducer = dashboardSlice.reducer;
export const dashboardActions = dashboardSlice.actions;
