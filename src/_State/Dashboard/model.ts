import { IClient, IStatistics } from '_Types/AppTypes';

export interface IDashboardState {
  statistics: IStatistics;
  clients: IClient[];
}

export interface IStateWithDashboard {
  dashboard: IDashboardState;
}
