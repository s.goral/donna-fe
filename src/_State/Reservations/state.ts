import {
  CaseReducer,
  createSelector,
  createSlice,
  PayloadAction,
} from '@reduxjs/toolkit';
import {
  IClient,
  IEmployee,
  IReservationDTO,
  ReservationStatus,
} from '_Types/AppTypes';
import { cloneDeep, identity } from 'lodash';
import { IOwner, IReservationsState, IStateWithReservations } from './model';
import {
  cancelReservation,
  getClients,
  getEmployees,
  getReservations,
} from '../../_Api/company.api';

const setReservations: CaseReducer<
  IReservationsState,
  PayloadAction<IReservationDTO[]>
> = (state, { payload }) => {
  const list = payload.filter(
    (reservation) =>
      reservation.status !== ReservationStatus.CANCELED &&
      reservation.status !== ReservationStatus.FINISHED
  );
  state.reservations = list;
  state.filteredReservations = list;
};

const setFilteredReservations: CaseReducer<
  IReservationsState,
  PayloadAction<any>
> = (state, { payload }) => {
  state.filteredReservations = payload;
};

const setClients: CaseReducer<IReservationsState, PayloadAction<IClient[]>> = (
  state,
  { payload }
) => {
  state.clients = payload;
};

const setEmployees: CaseReducer<
  IReservationsState,
  PayloadAction<IEmployee[]>
> = (state, { payload }) => {
  state.employees = payload;
};

const setOwners: CaseReducer<
  IReservationsState,
  PayloadAction<IReservationDTO[]>
> = (state, { payload }) => {
  state.owners = payload.map((reservation) => {
    return {
      text: reservation.ownerName,
      id: reservation.ownerId,
      color: '#83605e',
    };
  });
};

const INITIAL_STATE: IReservationsState = {
  reservations: [],
  filteredReservations: [],
  owners: [],
  activeReservationId: undefined,
  isAddDialogOpen: false,
  isEditDialogOpen: false,
  openingHour: 0,
  closingHour: 1,
  employees: [],
  clients: [],
  services: [],
};

const reservationsSlice = createSlice({
  name: 'reservations',
  initialState: INITIAL_STATE,
  reducers: {
    setReservations,
    setOwners,
    setEmployees,
    setClients,
    setFilteredReservations,
    setActiveReservationId: (state, { payload }) => {
      state.activeReservationId = payload;
    },
    setAddDialogOpen: (state, { payload }) => {
      state.isAddDialogOpen = payload;
    },
    setEditDialogOpen: (state, { payload }) => {
      state.isEditDialogOpen = payload;
    },
    setHours: (state, { payload: { openingHour, closingHour } }) => {
      state.closingHour = closingHour;
      state.openingHour = openingHour;
    },
    setServices: (state, { payload }) => {
      state.services = payload;
    },
    clearState: (state) => {
      Object.assign(state, cloneDeep(INITIAL_STATE));
    },
  },
});

export const getReservationsList = createSelector<
  IStateWithReservations,
  IReservationDTO[],
  IReservationDTO[]
>((state) => state.reservations.filteredReservations, identity);

export const getOwners = createSelector<
  IStateWithReservations,
  IOwner[],
  IOwner[]
>((state) => state.reservations.owners, identity);

export const getReservationsState = createSelector<
  IStateWithReservations,
  IReservationsState,
  IReservationsState
>((state) => state.reservations, identity);

export const filterReservations = (employee: IEmployee, client: IClient) => {
  return async (dispatch, getState) => {
    const {
      reservations: { reservations },
    } = getState();
    let list = reservations;
    if (client) {
      list = list.filter((reservation) => reservation.clientId === client.id);
    }
    if (employee) {
      list = list.filter((reservation) => reservation.ownerId === employee.id);
    }
    dispatch(reservationsSlice.actions.setFilteredReservations(list));
  };
};

export const loadReservationsData = () => {
  return async (dispatch) => {
    try {
      const {
        reservations,
        openingHour,
        closingHour,
        services,
      } = await getReservations();
      dispatch(reservationsSlice.actions.setReservations(reservations));
      dispatch(reservationsSlice.actions.setOwners(reservations));
      dispatch(
        reservationsSlice.actions.setHours({ openingHour, closingHour })
      );
      dispatch(reservationsSlice.actions.setServices(services));
    } finally {
    }
  };
};

export const loadAutocompleteData = () => {
  return async (dispatch) => {
    try {
      const clients = await getClients();
      const employees = await getEmployees();
      dispatch(reservationsSlice.actions.setClients(clients));
      dispatch(reservationsSlice.actions.setEmployees(employees));
    } finally {
    }
  };
};

export const cancelReservationThunk = (reservationId: number) => {
  return async (dispatch) => {
    try {
      await cancelReservation(reservationId);
      dispatch(loadReservationsData());
    } finally {
    }
  };
};

export const reservationsReducer = reservationsSlice.reducer;
export const reservationsActions = reservationsSlice.actions;
