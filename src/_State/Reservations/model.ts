import { IClient, IEmployee, IReservationDTO } from '_Types/AppTypes';

export interface IReservationsState {
  reservations: IReservationDTO[];
  filteredReservations: IReservationDTO[];
  owners: IOwner[];
  isAddDialogOpen: boolean;
  isEditDialogOpen: boolean;
  activeReservationId: number;
  services: string[];
  openingHour: number;
  closingHour: number;
  employees: IEmployee[];
  clients: IClient[];
}

export interface IStateWithReservations {
  reservations: IReservationsState;
}

export interface IOwner {
  text: string;
  id: number;
  color: string;
}
