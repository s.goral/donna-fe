import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { authReducer } from '_State/Auth/state';
import { userReducer } from '_State/User/state';
import { navigationPanelReducer } from '_State/NavigationPanel/state';
import { dashboardReducer } from '_State/Dashboard/state';
import { reservationsReducer } from '_State/Reservations/state';

const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  navigationPanel: navigationPanelReducer,
  dashboard: dashboardReducer,
  reservations: reservationsReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});
