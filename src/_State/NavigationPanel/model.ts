export interface INavigationPanelState {
  isOpen: boolean;
}

export interface IStateWithNavigationPanel {
  navigationPanel: INavigationPanelState;
}
