import { createSlice, CaseReducer, PayloadAction } from '@reduxjs/toolkit';
import { INavigationPanelState } from '_State/NavigationPanel/model';

const toggleOpen: CaseReducer<INavigationPanelState, PayloadAction<void>> = (
  state
) => {
  state.isOpen = !state.isOpen;
};

const navigationPanelSlice = createSlice({
  name: 'navigationPanel',
  initialState: {
    isOpen: true,
  } as INavigationPanelState,
  reducers: {
    toggleOpen,
  },
});

export const navigationPanelReducer = navigationPanelSlice.reducer;
export const navigationPanelActions = navigationPanelSlice.actions;
