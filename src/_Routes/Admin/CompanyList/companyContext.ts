import { createContext } from 'react';
import { ICompany } from '../../../_Types/AppTypes';

export interface ICompaniesContext {
  companies: ICompany[];
  deleteCompany?: (id?: number) => void;
}

export const defaultContext = {
  companies: [],
};

export const CompaniesContext = createContext<ICompaniesContext>(
  defaultContext
);
