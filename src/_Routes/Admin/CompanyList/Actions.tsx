import React, { useContext } from 'react';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { DnaIconButton } from '_Components/Buttons/IconButton/DnaIconButton';
import { Delete } from '@material-ui/icons';
import EditIcon from '@material-ui/icons/Edit';
import { useHistory } from 'react-router-dom';
import { CompaniesContext } from './companyContext';
const Wrapper = styled.div<IThemed>`
  display: flex;
  flex-direction: row;
`;
export const Actions = ({ id }) => {
  const { deleteCompany } = useContext(CompaniesContext);
  const history = useHistory();
  const handleDeleteClick = () => {
    deleteCompany(id);
  };
  return (
    <Wrapper>
      <DnaIconButton onClick={handleDeleteClick} iconSize={20}>
        <Delete />
      </DnaIconButton>
    </Wrapper>
  );
};
