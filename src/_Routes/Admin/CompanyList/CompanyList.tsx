import React, { useEffect, useMemo, useState } from 'react';
import { PageWrapper } from '_Components/PageWrapper';
import { useTranslation } from 'react-i18next';
import { DnaTable } from '_Components/table/DnaTable';
import { ICompany } from '_Types/AppTypes';
import { Actions } from './Actions';
import { CompaniesContext, ICompaniesContext } from './companyContext';
import { deleteCompany, getCompanies } from '../../../_Api/admin.api';
import styled from '@emotion/styled';
import { DnaInput } from '../../../_Components/DnaInput';

const CONFIG = [
  {
    label: 'id',
    labelKey: 'companyList.id',
    propKey: 'id',
  },
  {
    label: 'Name',
    labelKey: 'companyList.name',
    propKey: 'name',
  },
  {
    label: 'Clients',
    labelKey: 'companyList.clients',
    propKey: 'clientsAmount',
  },
  {
    label: 'Employees',
    labelKey: 'companyList.employees',
    propKey: 'employeesAmount',
  },
  {
    label: '',
    labelKey: 'clientList.actions',
    key: 'actions',
    className: 'actions-cell',
    cellRender: ({ id }: ICompany) => {
      return <Actions id={id} />;
    },
  },
];

const StyledInput = styled(DnaInput)`
  margin-bottom: 2rem;
  width: 50%;
`;

export const CompanyList = () => {
  const { t } = useTranslation();
  const [companies, setCompanies] = useState<ICompany[]>([]);
  const [name, setName] = useState('');
  const [filteredCompanies, setFilteredCompanies] = useState<ICompany[]>([]);
  const config = useMemo(() => {
    return CONFIG.map((config) => ({
      ...config,
      label: t(config.labelKey),
    }));
  }, [t]);
  const fetchCompanies = () => {
    getCompanies().then((companies) => {
      setCompanies(companies);
      setFilteredCompanies(companies);
    });
  };

  const handleNameChange = ({ target }) => {
    setName(target.value);
    setFilteredCompanies(
      companies.filter((company) => company.name.includes(target.value))
    );
  };

  useEffect(() => {
    fetchCompanies();
  }, []);

  const context: ICompaniesContext = {
    companies,
    deleteCompany: (id?: number) => {
      deleteCompany(id).then(() => fetchCompanies());
    },
  };

  return (
    <PageWrapper
      title={t('companyList.title')}
      titleText={t('companyList.info')}
    >
      <CompaniesContext.Provider value={context}>
        <StyledInput
          value={name}
          onChange={handleNameChange}
          label={'Search by name'}
        />
        <DnaTable configs={config} data={filteredCompanies} rowKey={'id'} />
      </CompaniesContext.Provider>
    </PageWrapper>
  );
};
