import { createContext } from 'react';
import { IUser } from '../../../_Types/AppTypes';

export interface IEmployeeContext {
  employees: IUser[];
  deleteEmployee?: (id?: number) => void;
}

export const defaultContext = {
  employees: [],
};

export const EmployeeContext = createContext<IEmployeeContext>(defaultContext);
