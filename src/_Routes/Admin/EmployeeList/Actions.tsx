import React, { useContext, useState } from 'react';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { DnaIconButton } from '_Components/Buttons/IconButton/DnaIconButton';
import { Delete } from '@material-ui/icons';
import { DnaButton } from '../../../_Components/Buttons/DnaButton';
import { DnaDialog } from '../../../_Components/Dialog/DnaDialog';
import { EmployeeContext } from './employeeContext';
import { useTranslation } from 'react-i18next';

const Wrapper = styled.div<IThemed>`
  display: flex;
  flex-direction: row;
`;
export const Actions = ({ active, id }) => {
  const { t } = useTranslation();
  const [dialogOpen, setDialogOpen] = useState(false);
  const { deleteEmployee } = useContext(EmployeeContext);
  const handleDeleteClick = () => {
    deleteEmployee(id);
  };

  const handleClose = () => {
    setDialogOpen(false);
  };
  return (
    <Wrapper>
      {active ? (
        <DnaIconButton onClick={() => setDialogOpen(true)} iconSize={20}>
          <Delete />
        </DnaIconButton>
      ) : (
        t('userList.notActive')
      )}
      <DnaDialog
        height={17}
        onClose={handleClose}
        isOpen={dialogOpen}
        title={t('userList.areYouSure')}
        actions={
          <>
            <DnaButton label={t('userList.yes')} onClick={handleDeleteClick} />
            <DnaButton label={t('userList.cancel')} onClick={handleClose} />
          </>
        }
      />
    </Wrapper>
  );
};
