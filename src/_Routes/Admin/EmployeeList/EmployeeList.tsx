import React, { useEffect, useMemo, useState } from 'react';
import { PageWrapper } from '_Components/PageWrapper';
import { useTranslation } from 'react-i18next';
import { DnaTable } from '_Components/table/DnaTable';
import { IUser } from '_Types/AppTypes';
import { Actions } from './Actions';
import { deleteAdminUser, getUsers } from '../../../_Api/admin.api';
import { IEmployeeContext, EmployeeContext } from './employeeContext';
import { DnaSelect } from '../../../_Components/DnaSelect';
import { EmployeeRolePicker } from './EmployeeRolePicker';
import styled from '@emotion/styled';
import { DnaInput } from '../../../_Components/DnaInput';

const CONFIG = [
  {
    label: 'id',
    labelKey: 'userList.id',
    propKey: 'id',
  },
  {
    label: 'Name',
    labelKey: 'userList.name',
    propKey: 'name',
  },
  {
    label: 'Last Name',
    labelKey: 'userList.lastName',
    propKey: 'lastName',
  },
  {
    label: 'isActive',
    labelKey: 'userList.isActive',
    key: 'isActive',
    cellRender: ({ active }: IUser) => {
      return <div>{active ? 'ACTIVE' : 'NOT ACTIVE'}</div>;
    },
  },
  {
    label: 'Email',
    labelKey: 'userList.email',
    propKey: 'email',
  },
  {
    label: 'Email',
    labelKey: 'userList.role',
    key: 'roleName',
    cellRender: ({ roleName, id }: IUser) => {
      return (
        <>
          <EmployeeRolePicker roleName={roleName} id={id} />
        </>
      );
    },
  },
  {
    label: '',
    labelKey: 'userList.actions',
    key: 'actions',
    className: 'actions-cell',
    cellRender: ({ active, id }: IUser) => {
      return <Actions active={active} id={id} />;
    },
  },
];

const StyledInput = styled(DnaInput)`
  margin-bottom: 2rem;
  width: 50%;
`;

export const EmployeeList = () => {
  const { t } = useTranslation();
  const [employees, setEmployees] = useState<IUser[]>([]);
  const [email, setEmail] = useState('');

  const [filteredUsers, setFilteredUsers] = useState<IUser[]>([]);
  const config = useMemo(() => {
    return CONFIG.map((config) => ({
      ...config,
      label: t(config.labelKey),
    }));
  }, [t]);

  const fetchUsers = () => {
    getUsers().then((users) => {
      setEmployees(users);
      setFilteredUsers(users);
    });
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  const context: IEmployeeContext = {
    employees,
    deleteEmployee: (id?: number) => {
      deleteAdminUser(id).then(() => fetchUsers());
    },
  };

  const handleEmailChange = ({ target }) => {
    setEmail(target.value);
    const filteredUsers = employees.filter((client) =>
      client.email.includes(target.value)
    );
    if (employees !== filteredUsers) {
      setFilteredUsers(filteredUsers);
    }
  };

  return (
    <PageWrapper title={t('userList.title')} titleText={t('userList.info')}>
      <EmployeeContext.Provider value={context}>
        <StyledInput
          value={email}
          onChange={handleEmailChange}
          label={t('userList.search')}
        />
        <DnaTable configs={config} data={filteredUsers} rowKey={'id'} />
      </EmployeeContext.Provider>
    </PageWrapper>
  );
};
