import React, { FC } from 'react';
import styled from '@emotion/styled';
import { NavigationPanel } from '_Routes/NavigationPanel/NavigationPanel';
import { Route } from 'react-router-dom';
import { IThemed } from '_Styles/types';
import { CompanyList } from './CompanyList/CompanyList';
import { ClientList } from './ClientList/ClientList';
import { EmployeeList } from './EmployeeList/EmployeeList';
import { EditClient } from '../Home/Client/EditClient/EditClient';

const Body = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
`;

const Main = styled.div<IThemed>`
  display: flex;
  flex-direction: column;
  position: relative;
  height: 100%;
  width: 100%;
  background-color: ${({ theme }) => theme.palette.background.default};
`;

export const Admin: FC = () => {
  return (
    <Body>
      <NavigationPanel />
      <Main>
        <Route exact path="/admin/companies">
          <CompanyList />
        </Route>
        <Route exact path="/admin/clients">
          <ClientList />
        </Route>
        <Route exact path="/admin/manage-users">
          <EmployeeList />
        </Route>
        <Route exact path="/admin/edit-client/:id">
          <EditClient isAdmin />
        </Route>
      </Main>
    </Body>
  );
};
