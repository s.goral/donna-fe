import React, { FC, useEffect } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { AuthClient } from '_Routes/Auth/AuthClient';
import { Home } from '_Routes/Home/Home';
import { useDispatch, useSelector } from 'react-redux';
import { IState } from '_State';
import { Admin } from './Admin/Admin';
import { getUserState } from '../_State/User/state';
import { RoleName } from '../_Types/AppTypes';
import { getUserDetails } from '../_State/Auth/state';

export const RoutesSwitcher: FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserDetails());
  }, []);
  const sessionToken = sessionStorage.getItem('access_token');
  const stateToken = useSelector((state: IState) => state.auth.token);
  const token = stateToken || sessionToken;
  const { user } = useSelector(getUserState);

  const isUserAdmin = user?.roleName === RoleName.ROLE_USER_ADMIN;
  const isUser = user?.roleName === RoleName.ROLE_USER || isUserAdmin;
  const isAdmin = user?.roleName === RoleName.ROLE_ADMIN;
  return (
    <Switch>
      {token && (
        <>
          {isUser && (
            <Route path="/">
              <Home />
            </Route>
          )}
          {isAdmin && (
            <Route path="/admin">
              <Admin />
            </Route>
          )}
        </>
      )}
      <Route path="/auth">
        <AuthClient />
      </Route>
      <Route>
        <Redirect
          to={{
            pathname: token ? '/home' : '/auth/login',
          }}
        />
      </Route>
    </Switch>
  );
};
