import React from 'react';

import { AppointmentTooltip } from '@devexpress/dx-react-scheduler-material-ui';
import styled from '@emotion/styled';
import { IconButton } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { useDispatch } from 'react-redux';
import { reservationsActions } from '../../../_State/Reservations/state';
const Container = styled.div`
  display: flex;
`;

export const Header = ({ appointmentData, ...restProps }) => {
  const dispatch = useDispatch();
  const handleEditClick = () => {
    dispatch(reservationsActions.setEditDialogOpen(true));
    dispatch(reservationsActions.setActiveReservationId(appointmentData.id));
  };
  return (
    <AppointmentTooltip.Header {...restProps} appointmentData={appointmentData}>
      <Container>
        <IconButton
          /* eslint-disable-next-line no-alert */
          onClick={handleEditClick}
        >
          <EditIcon />
        </IconButton>
      </Container>
    </AppointmentTooltip.Header>
  );
};
