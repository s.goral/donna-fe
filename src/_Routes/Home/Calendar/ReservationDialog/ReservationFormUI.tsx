import React, { FC, useEffect, useMemo, useState } from 'react';
import styled from '@emotion/styled';
import { AutocompleteFormWrapper } from '_Components/Forms/AutocompleteFormWrapper';
import { useSelector } from 'react-redux';
import { getReservationsState } from '../../../../_State/Reservations/state';
import { DateTimeFormWrapper } from '_Components/Forms/DateTimeFormWrapper';
import { useFormik } from 'formik';
import { DnaButton } from '../../../../_Components/Buttons/DnaButton';
import { addMinutes, format, isBefore, isEqual, isSameDay } from 'date-fns';
import { prepareRequiredFieldsValidation } from '../../../../_Components/Forms/utils';
import { useTranslation } from 'react-i18next';
import { IReservationDTO } from '../../../../_Types/AppTypes';
import { FindReservationDialog } from './FindReservationDialog';
import { css } from 'emotion';
import { DnaSelect } from '../../../../_Components/DnaSelect';
import { DnaInput } from '../../../../_Components/DnaInput';
import { FieldErrorMessage } from '../../../../_Components/Forms/FieldErrorMessage';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  justify-content: space-between;
`;

const ErrorWrapper = styled.div`
  height: 6rem;
  font-size: 16px;
  color: red;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const StyledSelect = styled(DnaSelect)``;

const StyledDate = styled(DateTimeFormWrapper)`
  width: 45%;
`;

const StartDateWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 45%;
`;

const TimeWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

const INITIAL_VALUES = {
  client: undefined,
  employee: undefined,
  startDate: '',
  endDate: '',
};

const REQUIRED_FIELDS = ['client', 'employee', 'endDate', 'startDate'];

interface IProps {
  reservation?: IReservationDTO;
  onSubmit: (form) => void;
  error?: string;
}

export const ReservationFormUI: FC<IProps> = ({
  reservation,
  onSubmit,
  error,
}) => {
  const { t } = useTranslation();
  const [isFindDialogOpen, setIsFindDialogOpen] = useState(false);
  const [startDate, setStartDate] = useState(reservation?.startDate);
  const [endDate, setEndDate] = useState(reservation?.endDate);
  const [client, setClient] = useState(null);
  const [service, setService] = useState('');
  const {
    employees,
    clients,
    closingHour,
    openingHour,
    services,
  } = useSelector(getReservationsState);
  const [employee, setEmployee] = useState(null);
  const requiredFieldsValidation = prepareRequiredFieldsValidation(
    REQUIRED_FIELDS,
    t('errors.reqField')
  );

  const serviceOptions = useMemo(
    () =>
      services.map((value) => {
        return {
          value,
          label: `${value}`,
          id: `${value}`,
        };
      }),
    [services]
  );

  const { handleSubmit, setFieldValue, errors, values, setValues } = useFormik({
    initialValues: INITIAL_VALUES,
    onSubmit: onSubmit,
    validate: (values) => {
      const errors = requiredFieldsValidation(values);
      if (
        isBefore(new Date(values.endDate), new Date(values.startDate)) ||
        isEqual(new Date(values.endDate), new Date(values.startDate))
      ) {
        errors.endDate = t('reservationDialog.dateError');
      } else if (
        values.endDate &&
        !isSameDay(new Date(values.endDate), new Date(values.startDate))
      ) {
        errors.endDate = t('reservationDialog.anotherDateError');
      } else if (new Date(values.startDate).getHours() < openingHour) {
        errors.startDate = t('reservationDialog.startDateError');
      } else if (new Date(values.endDate).getHours() > closingHour) {
        errors.endDate = t('reservationDialog.endDateError');
      }
      return errors;
    },
    validateOnMount: false,
    validateOnBlur: false,
    validateOnChange: false,
  });

  useEffect(() => {
    if (reservation) {
      const employeeValue = employees.find(
        (employee) => employee.id === reservation.ownerId
      );
      const clientValue = clients.find(
        (client) => client.id === reservation.clientId
      );
      setEmployee(employeeValue);
      setClient(clientValue);
      setService(reservation.service);
      setValues({
        client: clientValue,
        employee: employeeValue,
        startDate: format(new Date(reservation.startDate), 'yyyy-MM-dd HH:mm'),
        endDate: format(new Date(reservation.endDate), 'yyyy-MM-dd HH:mm'),
        service: reservation.service,
      });
    }
  }, [reservation, clients]);

  const handleFindDate = (date, length) => {
    setStartDate(format(new Date(date), 'yyyy-MM-dd HH:mm'));
    setEndDate(format(addMinutes(new Date(date), length), 'yyyy-MM-dd HH:mm'));
    setValues({
      ...values,
      startDate: format(new Date(date), 'yyyy-MM-dd HH:mm'),
      endDate: format(addMinutes(new Date(date), length), 'yyyy-MM-dd HH:mm'),
    });
  };

  return (
    <Wrapper>
      <AutocompleteFormWrapper
        value={employee}
        data={employees}
        label={t('reservationDialog.employee')}
        fieldName={'employee'}
        onChange={(value) => {
          setEmployee(value);
          setFieldValue('employee', value);
        }}
        error={errors.employee}
      />
      <AutocompleteFormWrapper
        value={client}
        data={clients}
        label={t('reservationDialog.client')}
        fieldName={'client'}
        onChange={(value) => {
          setClient(value);
          setFieldValue('client', value);
        }}
        error={errors.client}
      />
      <StyledSelect
        options={serviceOptions}
        label={'Service'}
        value={service}
        onChange={({ target }) => {
          setService(target.value);
          setFieldValue('service', target.value);
        }}
      />
      <FieldErrorMessage message={''} />
      <TimeWrapper>
        <StartDateWrapper>
          <DateTimeFormWrapper
            value={startDate ? startDate : null}
            onChange={setStartDate}
            label={t('reservationDialog.startDate')}
            fieldName={'startDate'}
            onSave={setFieldValue}
            error={errors.startDate}
          />
          {employee && (
            <DnaButton
              label={t('reservationDialog.next')}
              onClick={() => {
                setIsFindDialogOpen(true);
              }}
            />
          )}
        </StartDateWrapper>
        <StyledDate
          value={endDate ? endDate : null}
          onChange={setEndDate}
          label={t('reservationDialog.endDate')}
          fieldName={'endDate'}
          minDate={values.startDate}
          disabled={!values.startDate}
          onSave={setFieldValue}
          error={errors.endDate}
        />
      </TimeWrapper>

      <ErrorWrapper>{error}</ErrorWrapper>

      <FindReservationDialog
        isOpen={isFindDialogOpen}
        onClose={() => setIsFindDialogOpen(false)}
        onFind={handleFindDate}
        employeeId={employee?.id}
      />
      <DnaButton label={t('reservationDialog.save')} onClick={handleSubmit} />
    </Wrapper>
  );
};
