import React, { useEffect, useState } from 'react';
import { DnaDialog } from '../../../../_Components/Dialog/DnaDialog';
import { ReservationFormUI } from './ReservationFormUI';
import { useDispatch, useSelector } from 'react-redux';
import {
  getReservationsState,
  loadAutocompleteData,
  loadReservationsData,
  reservationsActions,
} from '../../../../_State/Reservations/state';
import { editReservation } from '../../../../_Api/company.api';
import { IReservationDTO } from '../../../../_Types/AppTypes';
import { useTranslation } from 'react-i18next';

export const EditReservationDialog = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();
  const { isEditDialogOpen, activeReservationId, reservations } = useSelector(
    getReservationsState
  );
  const [reservation, setReservation] = useState<IReservationDTO>(undefined);

  const [error, setError] = useState('');
  useEffect(() => {
    if (activeReservationId) {
      setReservation(
        reservations.find(
          (reservation) => reservation.id === activeReservationId
        )
      );
    }
  }, [activeReservationId, reservations]);

  const handleDialogClose = () => {
    dispatch(reservationsActions.setEditDialogOpen(false));
    setError('');
  };

  const handleSubmit = ({ employee, client, endDate, startDate, service }) => {
    editReservation(
      {
        startDate,
        endDate,
        clientId: client.id,
        employeeId: employee.id,
        service,
      },
      activeReservationId
    ).then(
      () => {
        dispatch(loadReservationsData());
        handleDialogClose();
      },
      (error) => {
        if (error.response.status === 409) {
          setError(t('reservationDialog.error'));
        }
      }
    );
  };
  return (
    <DnaDialog
      title={t('reservationDialog.title')}
      width={100}
      height={70}
      isOpen={isEditDialogOpen}
      onClose={handleDialogClose}
    >
      <ReservationFormUI
        error={error}
        reservation={reservation}
        onSubmit={handleSubmit}
      />
    </DnaDialog>
  );
};
