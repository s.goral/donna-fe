import React, { useEffect, useState } from 'react';
import { DnaDialog } from '../../../../_Components/Dialog/DnaDialog';
import { ReservationFormUI } from './ReservationFormUI';
import { useDispatch, useSelector } from 'react-redux';
import {
  getReservationsState,
  loadAutocompleteData,
  loadReservationsData,
  reservationsActions,
} from '../../../../_State/Reservations/state';
import { addNewReservation } from '../../../../_Api/company.api';
import { useTranslation } from 'react-i18next';

export const AddReservationDialog = () => {
  const dispatch = useDispatch();
  const { isAddDialogOpen, openingHour, closingHour } = useSelector(
    getReservationsState
  );
  const [error, setError] = useState('');

  const { t } = useTranslation();
  const handleDialogClose = () => {
    dispatch(reservationsActions.setAddDialogOpen(false));

    setError('');
  };

  const handleSubmit = ({ employee, client, endDate, startDate, service }) => {
    addNewReservation({
      startDate,
      endDate,
      clientId: client.id,
      employeeId: employee.id,
      service,
    }).then(
      () => {
        dispatch(loadReservationsData());
        handleDialogClose();
      },
      (error) => {
        if (error.response.status === 409) {
          setError(t('reservationDialog.error'));
        }
      }
    );
  };
  return (
    <DnaDialog
      title={t('reservationDialog.title')}
      width={100}
      height={70}
      isOpen={isAddDialogOpen}
      onClose={handleDialogClose}
    >
      <ReservationFormUI error={error} onSubmit={handleSubmit} />
    </DnaDialog>
  );
};
