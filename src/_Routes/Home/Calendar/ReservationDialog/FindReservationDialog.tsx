import React, { useState } from 'react';
import { DnaSelect } from '../../../../_Components/DnaSelect';
import { DnaButton } from '../../../../_Components/Buttons/DnaButton';
import { DnaDialog } from '../../../../_Components/Dialog/DnaDialog';
import styled from '@emotion/styled';
import { findReservation } from '../../../../_Api/company.api';
import { useTranslation } from 'react-i18next';
const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
export const FindReservationDialog = ({
  isOpen,
  onClose,
  employeeId,
  onFind,
}) => {
  const { t } = useTranslation();
  const [length, setLength] = useState(30);
  const handleSearchClick = () => {
    findReservation(length, employeeId).then((val) => {
      onFind(val, length);
      onClose();
    });
  };

  return (
    <DnaDialog
      isOpen={isOpen}
      height={20}
      width={40}
      onClose={onClose}
      title={t('reservationDialog.provide')}
    >
      <Container>
        <DnaSelect
          options={Array.from(Array(20).keys()).map((value) => {
            return {
              value: value * 15,
              label:
                value * 15 > 60
                  ? `${Math.floor((value * 15) / 60)} h ${
                      (value * 15) % 60
                    } min`
                  : `${value * 15} min`,
              id: `${value}`,
            };
          })}
          width={'100%'}
          label={'time'}
          value={length}
          onChange={(event) => {
            setLength(+event.target.value);
          }}
        />
        <DnaButton
          label={t('reservationDialog.search')}
          onClick={handleSearchClick}
        />
      </Container>
    </DnaDialog>
  );
};
