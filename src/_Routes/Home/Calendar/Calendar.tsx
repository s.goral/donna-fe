import React, { useEffect, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  DayView,
  Appointments,
  AppointmentTooltip,
  DateNavigator,
  Toolbar,
  ViewSwitcher,
  Resources,
  MonthView,
  CurrentTimeIndicator,
} from '@devexpress/dx-react-scheduler-material-ui';
import {
  filterReservations,
  getOwners,
  getReservationsList,
  getReservationsState,
  loadAutocompleteData,
  loadReservationsData,
  reservationsActions,
} from '../../../_State/Reservations/state';
import { useDispatch, useSelector } from 'react-redux';
import styled from '@emotion/styled';
import { IThemed } from '../../../_Styles/types';
import { themeGetters } from '../../../_Styles/themeHelpers';
import { DnaButton } from '../../../_Components/Buttons/DnaButton';
import { AddReservationDialog } from './ReservationDialog/AddReservationDialog';
import { EditReservationDialog } from './ReservationDialog/EditReservationDialog';
import { IReservation } from '../../../_Types/AppTypes';
import { Header } from './properties';
import { AutocompleteFormWrapper } from '../../../_Components/Forms/AutocompleteFormWrapper';
import { useTranslation } from 'react-i18next';

const { color } = themeGetters;

const StyledAutoComplete = styled(AutocompleteFormWrapper)`
  width: 30%;
  margin-left: 2rem;
`;

const CalendarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  margin: 5rem 10%;
`;

const InfoText = styled.div`
  margin-bottom: 1rem;
  display: flex;
  flex-direction: column;
  font-weight: 300;
  color: ${color('grey', 300)};
  font-size: 16px;
`;

const HeaderWrapper = styled.div<IThemed>`
  display: flex;
  flex-direction: column;
  font-size: 24px;
  font-weight: 500;
  color: ${color('primary', 'contrastText')};
  margin-bottom: 2rem;
`;

const HeaderContent = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 2rem;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;

export const Calendar = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const owners = useSelector(getOwners);
  const data = useSelector(getReservationsList);
  const { openingHour, closingHour } = useSelector(getReservationsState);
  const [client, setClient] = useState(null);
  const { employees, clients } = useSelector(getReservationsState);
  const [employee, setEmployee] = useState(null);
  const appointments: IReservation[] = JSON.parse(
    JSON.stringify(
      data.map((reservation) => ({
        ...reservation,
        title: `${reservation.clientName} ${reservation.service}`,
      }))
    )
  );
  const [resources, setResources] = useState(null);
  useEffect(() => {
    dispatch(loadReservationsData());
    dispatch(loadAutocompleteData());
    //eslint-disable-next-line
  }, []);
  useEffect(() => {
    dispatch(filterReservations(employee, client));
  }, [client, employee]);

  useEffect(() => {
    owners &&
      setResources([
        {
          fieldName: 'ownerId',
          title: 'Owners',
          instances: owners,
        },
      ]);
  }, [owners]);

  return (
    <CalendarWrapper>
      <HeaderWrapper>
        <InfoText>{t('calendar.title')}</InfoText>
        {t('calendar.info')}
        <HeaderContent>
          <DnaButton
            label={t('calendar.add')}
            onClick={() => {
              dispatch(reservationsActions.setAddDialogOpen(true));
            }}
          />
          <AddReservationDialog />
          <EditReservationDialog />
          <StyledAutoComplete
            value={employee}
            data={employees}
            label={t('calendar.employee')}
            fieldName={'employee'}
            onChange={(value) => {
              setEmployee(value);
            }}
          />
          <StyledAutoComplete
            value={client}
            data={clients}
            label={t('calendar.client')}
            fieldName={'client'}
            onChange={(value) => {
              setClient(value);
            }}
          />
        </HeaderContent>
      </HeaderWrapper>
      <Content>
        {resources && (
          <Paper>
            <Scheduler height={700} data={appointments}>
              <ViewState defaultCurrentDate={new Date()} />
              <DayView
                cellDuration={60}
                startDayHour={openingHour}
                endDayHour={closingHour}
              />
              <MonthView />
              <Appointments />
              <Toolbar />
              <ViewSwitcher />
              <DateNavigator />
              <AppointmentTooltip
                headerComponent={Header as any}
                showCloseButton
              />
              <Resources data={resources} />
              <CurrentTimeIndicator
                shadePreviousCells
                shadePreviousAppointments
                updateInterval={10000}
              />
            </Scheduler>
          </Paper>
        )}
      </Content>
    </CalendarWrapper>
  );
};
