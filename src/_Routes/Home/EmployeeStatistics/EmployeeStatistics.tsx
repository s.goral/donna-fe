import React, { useEffect, useState } from 'react';
import styled from '@emotion/styled';
import { DnaTilesPanel } from '_Components/DnaTilesPanel/DnaTilesPanel';
import { DnaInfoPanel } from '_Components/DnaInfoPanel/DnaInfoPanel';
import { useTranslation } from 'react-i18next';
import { DnaSimpleBarChart } from '_Components/Charts/BarCharts/DnaSimpleBarChart';
import { IThemed } from '../../../_Styles/types';
import { themeGetters } from '../../../_Styles/themeHelpers';
import { getEmployeeData } from '../../../_Api/company.api';
import { useParams } from 'react-router-dom';
import {
  barChartConfig,
  barWorkChartConfig,
  configureHoursMonthData,
  configureReservationsMonthData,
  createTilesConfig,
} from './configs';
import { IReservationDTO, ReservationStatus } from '../../../_Types/AppTypes';
import { ReservationChart } from './ReservationChart';
import { uniq } from 'lodash';
import { DnaSelect } from '../../../_Components/DnaSelect';

const StyledSelect = styled(DnaSelect)`
  && {
    position: absolute;
    top: 4px;
    right: 4px;
    background: transparent;
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`;
const { color } = themeGetters;
const InfoText = styled.div`
  margin-bottom: 1rem;
  display: flex;
  flex-direction: column;
  font-weight: 300;
  color: ${color('grey', 300)};
  font-size: 16px;
`;

const Header = styled.div<IThemed>`
  display: flex;
  flex-direction: column;
  font-size: 24px;
  font-weight: 500;
  color: ${color('primary', 'contrastText')};
  margin: 2rem 4rem 0 4rem;
`;

const ContentWrapper = styled.div`
  display: grid;
  grid-template-areas:
    'reservations-amount'
    'work-time'
    'reservations';
  grid-template-columns: 1fr;
  grid-template-rows: 1fr 1fr 1fr;
  column-gap: 2.5rem;
  row-gap: 2.5rem;
  padding: 4rem;
`;

const ReservationsContainer = styled.div`
  margin: 3rem;
  height: 100%;
`;

const StyledReservationsChart = styled(DnaInfoPanel)`
  grid-area: reservations-amount;
  position: relative;
`;

const StyledWorkTime = styled(DnaInfoPanel)`
  grid-area: work-time;
  position: relative;
`;

const StyledReservations = styled(DnaInfoPanel)`
  grid-area: reservations;
`;

export const EmployeeStatistics = () => {
  const { t } = useTranslation();
  const { employeeId } = useParams();
  const [reservations, setReservations] = useState<IReservationDTO[]>([]);
  const [year, setYear] = useState<any>(new Date().getFullYear());
  const [year2, setYear2] = useState<any>(new Date().getFullYear());

  const [statistics, setStatistics] = useState(undefined);
  const [name, setName] = useState('');

  useEffect(() => {
    getEmployeeData(employeeId).then(({ reservations, lastName, name }) => {
      setReservations(reservations);
      setName(`${name} ${lastName}`);
    });
  }, []);

  useEffect(() => {
    setStatistics({
      finishedReservations: reservations.filter(
        (r) => r.status === ReservationStatus.FINISHED
      ).length,
      totalReservations: reservations.length,
      canceledReservations: reservations.filter(
        (r) => r.status === ReservationStatus.CANCELED
      ).length,
      newReservations: reservations.filter(
        (r) => r.status === ReservationStatus.NEW
      ).length,
    });
  }, [reservations]);

  const tiles = createTilesConfig(statistics).concat([
    {
      component: <ReservationChart statistics={statistics} />,
    },
  ]);

  const reservationsYears = uniq(
    reservations
      .filter((r) => r.status === ReservationStatus.FINISHED)
      .map((c) => new Date(c.startDate).getFullYear())
  ).map((value) => {
    return {
      value,
      label: `${value}`,
      id: `${value}`,
    };
  });

  return (
    <Wrapper>
      <Header>
        <InfoText>Statystyki</InfoText>
        {name}

        {!reservations.length && <InfoText>No statistics data yet</InfoText>}
      </Header>
      {!!reservations.length && (
        <ContentWrapper>
          <StyledReservations label={t('dashboard.reservations')}>
            <ReservationsContainer>
              <DnaTilesPanel tiles={tiles} />
            </ReservationsContainer>
          </StyledReservations>
          <StyledWorkTime label={t('statistics.workTime')}>
            <StyledSelect
              label={t('statistics.year')}
              options={reservationsYears}
              value={year}
              onChange={({ target }) => setYear(target.value)}
            />
            <DnaSimpleBarChart
              config={barWorkChartConfig(
                configureHoursMonthData(
                  reservations.filter(
                    (r) => r.status === ReservationStatus.FINISHED
                  ),
                  year
                )
              )}
            />
          </StyledWorkTime>
          <StyledReservationsChart label={t('statistics.reservations')}>
            <StyledSelect
              label={t('statistics.year')}
              options={reservationsYears}
              value={year2}
              onChange={({ target }) => setYear2(target.value)}
            />
            <DnaSimpleBarChart
              config={barChartConfig(
                configureReservationsMonthData(
                  reservations.filter(
                    (r) => r.status === ReservationStatus.FINISHED
                  ),
                  year2
                )
              )}
            />
          </StyledReservationsChart>
        </ContentWrapper>
      )}
    </Wrapper>
  );
};
