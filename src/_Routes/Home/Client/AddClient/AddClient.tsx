import React, { useMemo, useState } from 'react';
import { PageWrapper } from '_Components/PageWrapper';
import { DateFormWrapper } from '_Components/Forms/DateFormWrapper';
import { InputFormWrapper } from '_Components/Forms/InputFormWrapper';
import { useFormik } from 'formik';
import { IClient } from '_Types/AppTypes';
import { prepareRequiredFieldsValidation } from '_Components/Forms/utils';
import { useTranslation } from 'react-i18next';
import { DnaButton } from '_Components/Buttons/DnaButton';
import { RadioGroupFormWrapper } from '_Components/Forms/RadioGroupFormWrapper';
import { Gender } from '_Types/types';
import { addNewClient } from '_Api/company.api';
import { DnaSnackbar } from '_Components/DnaSnackbar';
import { DnaDialog } from '_Components/Dialog/DnaDialog';
import { DialogType } from '_Components/Dialog/types';
import styled from '@emotion/styled';
import { useHistory } from 'react-router-dom';

const INITIAL_VALUES: Partial<IClient> = {
  name: '',
  lastName: '',
  phoneNumber: '',
  email: '',
  birthday: '',
  gender: undefined,
};

const options = [
  { value: Gender.FEMALE, label: 'addClient.female' },
  { value: Gender.MALE, label: 'addClient.male' },
];

const ActionsWrapper = styled.div`
  margin-top: 2rem;
`;

const REQUIRED_FIELDS = [
  'name',
  'lastName',
  'phoneNumber',
  'email',
  'birthday',
  'gender',
];

export const AddClient = () => {
  const { t } = useTranslation();
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const history = useHistory();

  const config = useMemo(
    () => options.map((option) => ({ ...option, label: t(option.label) })),
    [t]
  );

  const requiredFieldsValidation = prepareRequiredFieldsValidation(
    REQUIRED_FIELDS,
    t('errors.reqField')
  );
  const {
    handleSubmit,
    setFieldValue,
    handleChange,
    errors,
    handleBlur,
  } = useFormik({
    initialValues: INITIAL_VALUES,
    onSubmit: (form) => {
      addNewClient(form).then(
        () => {
          setSuccess(true);
        },
        () => {
          setError(true);
        }
      );
    },
    validate: (form) => {
      return requiredFieldsValidation(form);
    },
    validateOnMount: false,
    validateOnBlur: false,
    validateOnChange: false,
  });

  const handleDashboardClick = () => {
    history.push('/dashboard');
  };

  const handleAllClientsClick = () => {
    history.push('/clients');
  };

  const DialogActions = (
    <ActionsWrapper>
      <DnaButton
        label={t('addClient.goToDashboard')}
        onClick={handleDashboardClick}
      />
      <DnaButton
        label={t('addClient.goToClients')}
        onClick={handleAllClientsClick}
      />
    </ActionsWrapper>
  );

  return (
    <PageWrapper
      title={t('addClient.addNewClient')}
      titleText={t('addClient.client')}
    >
      <form onSubmit={handleSubmit}>
        <InputFormWrapper
          handleChange={handleChange}
          placeholder={t('addClient.namePlaceholder')}
          onBlur={handleBlur}
          fieldName={'name'}
          title={t('addClient.name')}
          error={errors.name}
        />
        <InputFormWrapper
          handleChange={handleChange}
          placeholder={t('addClient.lastNamePlaceholder')}
          fieldName={'lastName'}
          title={t('addClient.lastName')}
          error={errors.lastName}
        />
        <InputFormWrapper
          handleChange={handleChange}
          placeholder={t('addClient.phoneNumberPlaceholder')}
          fieldName={'phoneNumber'}
          title={t('addClient.phoneNumber')}
          error={errors.phoneNumber}
        />
        <InputFormWrapper
          handleChange={handleChange}
          placeholder={t('addClient.emailPlaceholder')}
          fieldName={'email'}
          title={t('addClient.email')}
          error={errors.email}
        />
        <DateFormWrapper
          title={t('addClient.birthday')}
          fieldName={'birthday'}
          onSave={setFieldValue}
          error={errors.birthday}
        />
        <RadioGroupFormWrapper
          options={config}
          fieldName={'gender'}
          title={t('addClient.gender')}
          setFieldValue={setFieldValue}
          error={errors.gender}
        />
        <DnaButton inputType={'submit'} label={t('addClient.save')} />
      </form>
      <DnaSnackbar
        type={'error'}
        open={error}
        onClose={() => setError(false)}
        message={t('addClient.problem')}
      />
      <DnaDialog
        type={DialogType.SUCCESS}
        isOpen={success}
        title={t('addClient.success')}
        infoText={t('addClient.info')}
        height={25}
        width={60}
        actions={DialogActions}
      />
    </PageWrapper>
  );
};
