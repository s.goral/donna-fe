import React, { useEffect, useMemo, useState } from 'react';
import { PageWrapper } from '_Components/PageWrapper';
import { DateFormWrapper } from '_Components/Forms/DateFormWrapper';
import { InputFormWrapper } from '_Components/Forms/InputFormWrapper';
import { useFormik } from 'formik';
import { IClient } from '_Types/AppTypes';
import { prepareRequiredFieldsValidation } from '_Components/Forms/utils';
import { useTranslation } from 'react-i18next';
import { DnaButton } from '_Components/Buttons/DnaButton';
import { RadioGroupFormWrapper } from '_Components/Forms/RadioGroupFormWrapper';
import { Gender } from '_Types/types';
import { getClient, updateClient } from '_Api/company.api';
import { DnaSnackbar } from '_Components/DnaSnackbar';
import { DnaDialog } from '_Components/Dialog/DnaDialog';
import { DialogType } from '_Components/Dialog/types';
import styled from '@emotion/styled';
import { useHistory, useParams } from 'react-router-dom';
import { getAdminClient, updateAdminClient } from '../../../../_Api/admin.api';

const INITIAL_VALUES: Partial<IClient> = {
  name: '',
  lastName: '',
  phoneNumber: '',
  email: '',
  birthday: '',
  gender: undefined,
};

const options = [
  { value: Gender.FEMALE, label: 'addClient.female' },
  { value: Gender.MALE, label: 'addClient.male' },
];

const ActionsWrapper = styled.div`
  margin-top: 2rem;
`;

const REQUIRED_FIELDS = [
  'name',
  'lastName',
  'phoneNumber',
  'email',
  'birthday',
  'gender',
];

export const EditClient = ({ isAdmin = false }) => {
  const { t } = useTranslation();
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [clientData, setClientData] = useState(null);
  const history = useHistory();
  const { id } = useParams();

  const config = useMemo(
    () => options.map((option) => ({ ...option, label: t(option.label) })),
    [t]
  );

  const requiredFieldsValidation = prepareRequiredFieldsValidation(
    REQUIRED_FIELDS,
    t('errors.reqField')
  );
  const {
    handleSubmit,
    setFieldValue,
    setValues,
    handleChange,
    errors,
    handleBlur,
  } = useFormik({
    initialValues: INITIAL_VALUES,
    onSubmit: (form) => {
      isAdmin
        ? updateAdminClient(form, id).then(() => {
            history.push('/admin/clients');
          })
        : updateClient(form, id).then(
            () => {
              setSuccess(true);
            },
            () => {
              setError(true);
            }
          );
    },
    validate: (form) => {
      return requiredFieldsValidation(form);
    },
    validateOnMount: false,
    validateOnBlur: false,
    validateOnChange: false,
  });

  useEffect(() => {
    isAdmin
      ? getAdminClient(id).then((client) => {
          setClientData(client);
          setValues(client);
        })
      : getClient(id).then((client) => {
          setClientData(client);
          setValues(client);
        });
  }, []);

  const handleDashboardClick = () => {
    history.push('/dashboard');
  };

  const handleAllClientsClick = () => {
    history.push('/clients');
  };

  const DialogActions = (
    <ActionsWrapper>
      <DnaButton
        label={t('addClient.goToDashboard')}
        onClick={handleDashboardClick}
      />
      <DnaButton
        label={t('addClient.goToClients')}
        onClick={handleAllClientsClick}
      />
    </ActionsWrapper>
  );

  return (
    <PageWrapper
      title={t('editClient.editClient')}
      titleText={t('editClient.client')}
    >
      <form onSubmit={handleSubmit}>
        <InputFormWrapper
          value={clientData?.name}
          handleChange={handleChange}
          placeholder={t('editClient.namePlaceholder')}
          onBlur={handleBlur}
          fieldName={'name'}
          title={t('editClient.name')}
          error={errors.name}
        />
        <InputFormWrapper
          value={clientData?.lastName}
          handleChange={handleChange}
          placeholder={t('editClient.lastNamePlaceholder')}
          fieldName={'lastName'}
          title={t('editClient.lastName')}
          error={errors.lastName}
        />
        <InputFormWrapper
          value={clientData?.phoneNumber}
          handleChange={handleChange}
          placeholder={t('editClient.phoneNumberPlaceholder')}
          fieldName={'phoneNumber'}
          title={t('editClient.phoneNumber')}
          error={errors.phoneNumber}
        />
        <InputFormWrapper
          value={clientData?.email}
          handleChange={handleChange}
          placeholder={t('editClient.emailPlaceholder')}
          fieldName={'email'}
          title={t('editClient.email')}
          error={errors.email}
        />
        <DateFormWrapper
          value={clientData?.birthday}
          title={t('editClient.birthday')}
          fieldName={'birthday'}
          onSave={setFieldValue}
          error={errors.birthday}
        />
        <RadioGroupFormWrapper
          value={clientData?.gender}
          options={config}
          fieldName={'gender'}
          title={t('editClient.gender')}
          setFieldValue={setFieldValue}
          error={errors.gender}
        />
        <DnaButton inputType={'submit'} label={t('editClient.save')} />
      </form>
      {!isAdmin && (
        <>
          {' '}
          <DnaSnackbar
            type={'error'}
            open={error}
            onClose={() => setError(false)}
            message={t('editClient.problem')}
          />
          <DnaDialog
            type={DialogType.SUCCESS}
            isOpen={success}
            title={t('editClient.success')}
            infoText={t('editClient.info')}
            height={25}
            width={60}
            actions={DialogActions}
          />
        </>
      )}
    </PageWrapper>
  );
};
