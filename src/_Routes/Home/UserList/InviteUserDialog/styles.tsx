import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useDialogStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      width: 600,
    },
    content: {
      padding: theme.spacing(2, 4, 3.6, 5),
    },
    actions: {
      borderTop: `3px solid ${theme.palette.primary.light}`,
      padding: theme.spacing(1.5, 2),
    },
  })
);
