import React, { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DnaSelect } from '../../../../../_Components/DnaSelect';
import styled from '@emotion/styled';
import { DnaInput } from '../../../../../_Components/DnaInput';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  margin: 2rem 0;
`;

const StyledInput = styled(DnaInput)`
  width: 65%;
`;

interface IProps {
  inputName: string;
  selectName: string;
  onSave: (
    inputName: string,
    selectValue: unknown,
    inputValue: string | number | string[]
  ) => void;
}
const roles = [
  {
    value: 'ROLE_USER_ADMIN',
    label: 'ADMIN',
    id: 'ADMIN',
  },
  {
    value: 'ROLE_USER',
    label: 'EMPLOYEE',
    id: 'EMPLOYEE',
  },
];
export const UserField: FC<IProps> = ({ inputName, selectName, onSave }) => {
  const { t } = useTranslation();
  const [selectValue, setSelectValue] = useState(roles[1].value);
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    if (selectValue && inputValue) {
      onSave(inputName, selectValue, inputValue);
    }
  }, [selectValue, inputValue]);

  const handleSelectChange = ({ target }) => {
    setSelectValue(target.value);
  };

  const handleInputChange = ({ target }) => {
    setInputValue(target.value);
  };

  return (
    <Wrapper>
      <StyledInput
        value={inputValue}
        onChange={handleInputChange}
        label={'Email'}
      />
      <DnaSelect
        label={'Role'}
        value={selectValue}
        onChange={handleSelectChange}
        options={roles}
      />
    </Wrapper>
  );
};
