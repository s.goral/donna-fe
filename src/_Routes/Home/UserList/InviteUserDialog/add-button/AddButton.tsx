import React, { FC } from 'react';
import styled from '@emotion/styled';
import { themeGetters } from '../../../../../_Styles/themeHelpers';
import { IThemed } from '../../../../../_Styles/types';
import AddBoxIcon from '@material-ui/icons/AddBox';

const { color, spacing } = themeGetters;

const Label = styled.div<IThemed>`
  font-size: 15px;
  font-weight: 500;
  color: ${color('secondary')};
  padding-left: ${spacing(0.5)};
`;

const Wrapper = styled.div<IThemed>`
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;
  margin-top: ${spacing(2)};
`;

interface IProps {
  label: string;
  onClick: () => void;
}

export const AddButton: FC<IProps> = ({ label, onClick }) => {
  return (
    <Wrapper onClick={onClick}>
      <AddBoxIcon />
      <Label>{label}</Label>
    </Wrapper>
  );
};
