import React, { FC, useState, useRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import styled from '@emotion/styled';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { AddButton } from './add-button/AddButton';
import { useDialogStyles } from './styles';
import { UserField } from './user-field/UserField';
import { DnaButton } from '../../../../_Components/Buttons/DnaButton';
import { IThemed } from '../../../../_Styles/types';
import { themeGetters } from '../../../../_Styles/themeHelpers';

interface IProps {
  open: boolean;
  onClose: () => void;
  onInvite: (users: IUser[]) => void;
}

interface IUser {
  userName?: string;
  email: string;
  role: string;
}

const { spacing, color } = themeGetters;

const Title = styled.div<IThemed>`
  padding: ${spacing(1, 0, 2, 0)};
  font-size: 16px;
  font-weight: 500;
  color: ${color('secondary')};
`;

const UsersFields = styled.div<IThemed>`
  overflow-y: auto;
  padding-right: ${spacing(1)};
  display: flex;
  flex-direction: column;
  height: ${spacing(27)};
`;

export const InviteUserDialog: FC<IProps> = ({ open, onClose, onInvite }) => {
  const [users, setUser] = useState<IUser[]>([]);
  const [usersAmount, setUserAmount] = useState<number[]>([0, 1, 2]);
  const { t } = useTranslation();
  const classes = useDialogStyles();
  const divRef = useRef(null);

  const { paper } = classes;

  const handleInvite = () => {
    const userInvitations = users
      .filter(({ email }) => {
        return email !== '' && email;
      })
      .map(({ email, role }) => ({
        email: email.toLowerCase(),
        role,
      }));
    onInvite(userInvitations);
    onClose();
  };

  useEffect(() => {
    if (divRef.current) {
      divRef.current.scrollIntoView({ block: 'end', behavior: 'smooth' });
    }
  }, [usersAmount]);

  const generateNewField = () => {
    setUserAmount((elements) => [...elements, elements.length++]);
  };

  const handleSelectInputChange = (
    userName: string,
    role: string,
    email: string
  ) => {
    const user: IUser = {
      userName,
      email,
      role: role as string,
    };

    if (users.some((user) => user.userName === userName)) {
      const newUsers = users.filter((u) => u.userName !== userName);
      setUser([...newUsers, user]);
    } else {
      setUser((users) => [...users, user]);
    }
  };

  return (
    <Dialog
      classes={{ paper }}
      open={open}
      onClose={onClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogContent className={classes.content}>
        <Title>{t('userList.dialogTitle')}</Title>
        <UsersFields>
          {usersAmount.map((i) => (
            <UserField
              inputName={'user-' + i}
              selectName={'user-' + i}
              onSave={handleSelectInputChange}
              key={i}
            />
          ))}
          <div ref={divRef} />
        </UsersFields>
        <AddButton
          label={t('userList.addAnother')}
          onClick={generateNewField}
        />
      </DialogContent>
      <DialogActions className={classes.actions}>
        <DnaButton onClick={onClose} label={t('userList.cancel')} />
        <DnaButton onClick={handleInvite} label={t('userList.send')} />
      </DialogActions>
    </Dialog>
  );
};
