import React, { useState } from 'react';
import { DnaSelect } from '../../../_Components/DnaSelect';
import styled from '@emotion/styled';
import { updateAdminRoleName } from '../../../_Api/admin.api';
import { updateRoleName } from '../../../_Api/company.api';
import { useTranslation } from 'react-i18next';

const roles = [
  {
    value: 'ROLE_USER_ADMIN',
    label: 'ADMIN',
    id: 'ADMIN',
  },
  {
    value: 'ROLE_USER',
    label: 'EMPLOYEE',
    id: 'EMPLOYEE',
  },
];

export const EmployeeRolePicker = ({ roleName, id }) => {
  const { t } = useTranslation();
  const [selectValue, setSelectValue] = useState(roleName);
  const handleSelectChange = ({ target }) => {
    setSelectValue(target.value);
    updateRoleName(target.value, id).then();
  };
  return (
    <>
      <DnaSelect
        label={t('userList.role')}
        value={selectValue}
        onChange={handleSelectChange}
        options={roles}
        width={'100%'}
      />
    </>
  );
};
