import { createContext } from 'react';
import { IUser } from '../../../_Types/AppTypes';

export interface IUsersContext {
  users: IUser[];
  deleteUser?: (userId?: number) => void;
}

export const defaultContext = {
  users: [],
};

export const UsersContext = createContext<IUsersContext>(defaultContext);
