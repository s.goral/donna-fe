import React, { useContext } from 'react';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { DnaIconButton } from '_Components/Buttons/IconButton/DnaIconButton';
import { Delete } from '@material-ui/icons';
import { UsersContext } from './usersContext';
import { useTranslation } from 'react-i18next';
import BarChartIcon from '@material-ui/icons/BarChart';
import { useHistory } from 'react-router-dom';

const Wrapper = styled.div<IThemed>`
  display: flex;
  flex-direction: row;
`;
export const Actions = ({ active, id }) => {
  const { t } = useTranslation();
  const { deleteUser } = useContext(UsersContext);
  const history = useHistory();

  const handleDeleteClick = () => {
    deleteUser(id);
  };
  return (
    <Wrapper>
      {active ? (
        <>
          <DnaIconButton onClick={handleDeleteClick} iconSize={20}>
            <Delete />
          </DnaIconButton>
          <DnaIconButton
            onClick={() => history.push(`employee/${id}/statistics`)}
            iconSize={20}
          >
            <BarChartIcon />
          </DnaIconButton>
        </>
      ) : (
        t('userList.notActive')
      )}
    </Wrapper>
  );
};
