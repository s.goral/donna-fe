import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DnaTable } from '_Components/table/DnaTable';
import { IUser } from '_Types/AppTypes';
import { getUsers, inviteUsers, deleteUser } from '_Api/company.api';
import { Actions } from './Actions';
import { useSelector } from 'react-redux';
import { getUserState } from '../../../_State/User/state';
import styled from '@emotion/styled';
import { DnaButton } from '../../../_Components/Buttons/DnaButton';
import { IThemed } from '../../../_Styles/types';
import { themeGetters } from '../../../_Styles/themeHelpers';
import { InviteUserDialog } from './InviteUserDialog/InviteUserDialog';
import { IUsersContext, UsersContext } from './usersContext';
import { EmployeeRolePicker } from './EmployeeRolePicker';
import { DnaInput } from '../../../_Components/DnaInput';

const { color } = themeGetters;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  margin: 5rem 10%;
`;

const InfoText = styled.div`
  margin-bottom: 1rem;
  display: flex;
  flex-direction: column;
  font-weight: 300;
  color: ${color('grey', 300)};
  font-size: 16px;
`;

const HeaderWrapper = styled.div<IThemed>`
  display: flex;
  flex-direction: column;
  font-size: 24px;
  font-weight: 500;
  color: ${color('primary', 'contrastText')};
  margin-bottom: 2rem;
`;

const HeaderContent = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 2rem;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;
const StyledInput = styled(DnaInput)`
  margin-bottom: 2rem;
  width: 50%;
`;

const CONFIG = [
  {
    label: 'Name',
    labelKey: 'userList.name',
    propKey: 'name',
  },
  {
    label: 'Last Name',
    labelKey: 'userList.lastName',
    propKey: 'lastName',
  },
  {
    label: 'Email',
    labelKey: 'userList.email',
    propKey: 'email',
  },
  {
    label: 'role',
    labelKey: '.role',
    key: 'roleName',
    cellRender: ({ roleName, id }: IUser) => {
      return (
        <>
          <EmployeeRolePicker roleName={roleName} id={id} />
        </>
      );
    },
  },
  {
    label: '',
    labelKey: 'clientList.actions',
    key: 'actions',
    className: 'actions-cell',
    cellRender: ({ active, id }: any) => {
      return <Actions active={active} id={id} />;
    },
  },
];

export const UserList = () => {
  const { t } = useTranslation();
  const [users, setUsers] = useState<IUser[]>([]);
  const { user } = useSelector(getUserState);
  const [filteredUsers, setFilteredUsers] = useState<IUser[]>([]);
  const [email, setEmail] = useState('');
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const config = useMemo(() => {
    return CONFIG.map((config) => ({
      ...config,
      label: t(config.labelKey),
    }));
  }, [t]);

  const fetchUsers = () => {
    getUsers().then((users) => {
      setUsers(users.filter((u) => u.id !== user.id));
      setFilteredUsers(users.filter((u) => u.id !== user.id));
    });
  };

  useEffect(() => {
    user && fetchUsers();
  }, [user]);

  const handleOnInvite = (users) => {
    inviteUsers(users).then(() => {
      fetchUsers();
    });
  };

  const context: IUsersContext = {
    users: users,
    deleteUser: (userId) => {
      deleteUser(userId).then(() => fetchUsers());
    },
  };

  const handleEmailChange = ({ target }) => {
    setEmail(target.value);
    const filteredUsers = users.filter((client) =>
      client.email.includes(target.value)
    );
    if (users !== filteredUsers) {
      setFilteredUsers(filteredUsers);
    }
  };

  return (
    <Wrapper>
      <HeaderWrapper>
        <InfoText>{t('userList.title')}</InfoText>
        {t('userList.info')}
        <HeaderContent>
          <DnaButton
            label={t('userList.invite')}
            onClick={() => {
              setIsDialogOpen(true);
            }}
          />
          <InviteUserDialog
            open={isDialogOpen}
            onClose={() => setIsDialogOpen(false)}
            onInvite={handleOnInvite}
          />
        </HeaderContent>
      </HeaderWrapper>
      <UsersContext.Provider value={context}>
        <StyledInput
          value={email}
          onChange={handleEmailChange}
          label={t('userList.search')}
        />
        <DnaTable configs={config} data={filteredUsers} rowKey={'id'} />
      </UsersContext.Provider>
    </Wrapper>
  );
};
