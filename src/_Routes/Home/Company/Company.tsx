import React, { useEffect, useState } from 'react';
import { PageWrapper } from '_Components/PageWrapper';
import { InputFormWrapper } from '_Components/Forms/InputFormWrapper';
import { useFormik } from 'formik';
import { prepareRequiredFieldsValidation } from '_Components/Forms/utils';
import { useTranslation } from 'react-i18next';
import { DnaButton } from '_Components/Buttons/DnaButton';
import { addNewClient, getCompanyData, updateCompany } from '_Api/company.api';
import { DnaSnackbar } from '_Components/DnaSnackbar';
import { DnaDialog } from '_Components/Dialog/DnaDialog';
import { DialogType } from '_Components/Dialog/types';
import { FieldErrorMessage } from '../../../_Components/Forms/FieldErrorMessage';
import styled from '@emotion/styled';
import { DnaSelect } from '../../../_Components/DnaSelect';

const StyledSelect = styled(DnaSelect)`
  background-color: white;
  width: 100%;
`;

const INITIAL_VALUES = {
  name: '',
  openingHour: '',
  closingHour: '',
  services: [],
};

const REQUIRED_FIELDS = ['name', 'openingHour', 'closingHour'];

const selectOptions = Array.from(Array(24).keys()).map((value) => {
  return {
    value,
    label: `${value}`,
    id: `${value}`,
  };
});

export const Company = () => {
  const { t } = useTranslation();
  const [success, setSuccess] = useState(false);
  const [openingHour, setOpeningHour] = useState(8);
  const [closingHour, setClosingHour] = useState(20);
  const [company, setCompany] = useState(undefined);
  const [numChildren, setNumChildren] = useState(1);
  const [services, setServices] = useState([]);

  const requiredFieldsValidation = prepareRequiredFieldsValidation(
    REQUIRED_FIELDS,
    t('errors.reqField')
  );
  const {
    handleSubmit,
    setFieldValue,
    handleChange,
    errors,
    setValues,
    handleBlur,
  } = useFormik({
    initialValues: INITIAL_VALUES,
    onSubmit: (form) => {
      updateCompany(form).then(() => {
        setSuccess(true);
      });
    },
    validate: (form) => {
      return requiredFieldsValidation(form);
    },
    validateOnMount: false,
    validateOnBlur: false,
    validateOnChange: false,
  });
  useEffect(() => {
    getCompanyData().then((values) => {
      setCompany(values);
      setValues(values);
      setOpeningHour(values.openingHour);
      setClosingHour(values.closingHour);
      setServices(values.services);
      setNumChildren(values.services.length);
    });
  }, []);

  const handleSelectChange = (name, event, setValue, setter) => {
    const { value } = event.target;
    setter(value);
    setValue(name, value);
  };

  return (
    <PageWrapper
      title={t('company.company')}
      titleText={t('company.updateCompanyData')}
    >
      <form onSubmit={handleSubmit}>
        <InputFormWrapper
          handleChange={handleChange}
          value={company?.name}
          placeholder={t('company.namePlaceholder')}
          onBlur={handleBlur}
          fieldName={'name'}
          title={t('company.name')}
          error={errors.name}
        />
        <StyledSelect
          options={selectOptions}
          label={t('company.openingHour')}
          value={openingHour}
          onChange={(event) => {
            handleSelectChange(
              'openingHour',
              event,
              setFieldValue,
              setOpeningHour
            );
          }}
        />
        <FieldErrorMessage message={errors.openingHour} />
        <StyledSelect
          options={selectOptions}
          label={t('company.closingHour')}
          value={closingHour}
          onChange={(event) => {
            handleSelectChange(
              'closingHour',
              event,
              setFieldValue,
              setClosingHour
            );
          }}
        />
        {Array.from({ length: numChildren }, (_, i) => (
          <>
            <InputFormWrapper
              handleChange={handleChange}
              value={services[i]}
              placeholder={t('company.servicePlaceholder')}
              onBlur={handleBlur}
              fieldName={`services[${i}]`}
              title={`${t('company.service')} ${i + 1}`}
              error={errors.name}
            />
          </>
        ))}
        <DnaButton
          label={'add'}
          onClick={() => {
            setNumChildren(numChildren + 1);
          }}
        />
        <FieldErrorMessage message={errors.closingHour} />
        <DnaButton inputType={'submit'} label={t('company.save')} />
      </form>
      <DnaDialog
        type={DialogType.SUCCESS}
        isOpen={success}
        title={t('company.success')}
        infoText={t('company.info')}
        onClose={() => setSuccess(false)}
        height={15}
        width={40}
      />
    </PageWrapper>
  );
};
