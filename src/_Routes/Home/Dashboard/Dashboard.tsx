import React, { useEffect, useState } from 'react';
import styled from '@emotion/styled';
import { DnaTilesPanel } from '_Components/DnaTilesPanel/DnaTilesPanel';
import {
  barChartConfig,
  configureMonthData,
  createTilesConfig,
} from '_Routes/Home/Dashboard/configs';
import { DnaInfoPanel } from '_Components/DnaInfoPanel/DnaInfoPanel';
import { useTranslation } from 'react-i18next';
import { ReservationChart } from '_Routes/Home/Dashboard/ReservationChart';
import { DnaSimpleBarChart } from '_Components/Charts/BarCharts/DnaSimpleBarChart';
import { useDispatch, useSelector } from 'react-redux';
import {
  dashboardActions,
  getDashboardState,
  loadDashboardData,
} from '_State/Dashboard/state';
import { DnaSelect } from '../../../_Components/DnaSelect';
import { uniq } from 'lodash';

const StyledSelect = styled(DnaSelect)`
  && {
    position: absolute;
    top: 4px;
    right: 4px;
    background: transparent;
  }
`;

const ContentWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-areas:
    'reservations'
    'clients';
  grid-template-columns: 1fr;
  grid-template-rows: 30rem 1fr;
  column-gap: 2.5rem;
  row-gap: 2.5rem;
  padding: 4rem;
`;

const ReservationsContainer = styled.div`
  margin: 3rem;
  height: 100%;
`;

const StyledClientsChart = styled(DnaInfoPanel)`
  grid-area: clients;
  position: relative;
`;

const StyledReservations = styled(DnaInfoPanel)`
  grid-area: reservations;
`;

export const Dashboard = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [year, setYear] = useState<any>(new Date().getFullYear());
  const { statistics, clients } = useSelector(getDashboardState) || {};

  useEffect(() => {
    dispatch(loadDashboardData());
    return () => {
      dispatch(dashboardActions.clearState());
    };
  }, [dispatch]);

  const tiles = createTilesConfig(statistics).concat([
    {
      component: <ReservationChart />,
    },
  ]);

  const clientsYears = uniq(
    clients.map((c) => new Date(c.joinDate).getFullYear())
  ).map((value) => {
    return {
      value,
      label: `${value}`,
      id: `${value}`,
    };
  });

  return (
    <ContentWrapper>
      <StyledReservations label={t('dashboard.reservations')}>
        <ReservationsContainer>
          <DnaTilesPanel tiles={tiles} />
        </ReservationsContainer>
      </StyledReservations>
      <StyledClientsChart label={t('dashboard.clients')}>
        <StyledSelect
          label={'year'}
          options={clientsYears}
          value={year}
          onChange={({ target }) => setYear(target.value)}
        />
        <DnaSimpleBarChart
          config={barChartConfig(configureMonthData(clients, year))}
        />
      </StyledClientsChart>
    </ContentWrapper>
  );
};
