import { IClient, IStatistics } from '_Types/AppTypes';
import i18next from 'i18next';
import { ITile } from '_Types/ITile';
import moment from 'moment';

export const createTilesConfig = (statistics: IStatistics): ITile[] => {
  return [
    {
      label: i18next.t('dashboard.totalReservations'),
      amount: statistics?.totalReservations,
    },
    {
      label: i18next.t('dashboard.canceled'),
      amount: statistics?.canceledReservations,
    },
    {
      label: i18next.t('dashboard.new'),
      amount: statistics?.newReservations,
    },
    {
      label: i18next.t('dashboard.finished'),
      amount: statistics?.finishedReservations,
    },
  ];
};

const countClientsByMonth = (
  clients: IClient[],
  month: number,
  year
): number => {
  return clients.filter(
    (client) =>
      moment(client.joinDate).month() + 1 === month &&
      moment(client.joinDate).year() === year
  ).length;
};

export const configureMonthData = (clients: IClient[], year): number[] => {
  return Array.from(Array(12).keys()).map((value) =>
    countClientsByMonth(clients, value + 1, year)
  );
};

export const barChartConfig = (data, interval = 1) => ({
  tooltip: { formatter: ' {c} clients', backgroundColor: '#192044' },
  grid: { left: 65, right: 50, top: 50, bottom: 30 },
  xAxis: {
    type: 'category',
    data: [
      i18next.t('months.january'),
      i18next.t('months.february'),
      i18next.t('months.march'),
      i18next.t('months.april'),
      i18next.t('months.may'),
      i18next.t('months.june'),
      i18next.t('months.july'),
      i18next.t('months.august'),
      i18next.t('months.september'),
      i18next.t('months.october'),
      i18next.t('months.november'),
      i18next.t('months.december'),
    ],
    axisTick: { show: false },
    axisLine: { show: false },
  },
  yAxis: {
    type: 'value',
    axisTick: { show: false },
    axisLine: { show: false },
    interval,
  },
  series: [
    {
      barWidth: 40,
      data: data,
      type: 'bar',
      itemStyle: { barBorderRadius: 4, color: '#b2857c' },
    },
  ],
});

export const doughnutOption = (
  newReservations,
  canceledReservations,
  finishedReservations
) => ({
  tooltip: {
    formatter: ' {d}% {b}',
  },
  series: [
    {
      name: 'Reservation',
      type: 'pie',
      radius: ['50%', '100%'],
      avoidLabelOverlap: true,
      label: {
        show: false,
        position: 'center',
      },
      animation: false,
      labelLine: {
        show: false,
      },
      data: [
        {
          value: canceledReservations,
          name: i18next.t('dashboard.canceled'),
          itemStyle: { color: '#5f0300' },
        },
        {
          value: newReservations,
          name: i18next.t('dashboard.new'),
          itemStyle: { color: '#79b276' },
        },
        {
          value: finishedReservations,
          name: i18next.t('dashboard.finished'),
          itemStyle: { color: '#b2857c' },
        },
      ],
    },
  ],
});
