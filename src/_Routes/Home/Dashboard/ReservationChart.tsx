import React, { useEffect, useState } from 'react';
import { DnaDoughnutChart } from '_Components/Charts/DnaDoughnutChart/DnaDoughnutChart';
import { doughnutOption } from '_Routes/Home/Dashboard/configs';
import { useSelector } from 'react-redux';
import { getDashboardState } from '_State/Dashboard/state';

export const ReservationChart = () => {
  const [options, setOptions] = useState({});
  const { statistics } = useSelector(getDashboardState);
  useEffect(() => {
    setOptions(
      JSON.parse(
        JSON.stringify(
          doughnutOption(
            statistics?.newReservations,
            statistics?.canceledReservations,
            statistics?.finishedReservations
          )
        )
      )
    );
  }, [statistics]);

  return <DnaDoughnutChart option={options} />;
};
