import { createContext } from 'react';
import { IClient } from '../../../_Types/AppTypes';

export interface IClientsContext {
  clients: IClient[];
  deleteClient?: (clientId?: number) => void;
}

export const defaultContext = {
  clients: [],
};

export const ClientsContext = createContext<IClientsContext>(defaultContext);
