import React, { useContext, useState } from 'react';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { DnaIconButton } from '_Components/Buttons/IconButton/DnaIconButton';
import { Delete } from '@material-ui/icons';
import EditIcon from '@material-ui/icons/Edit';
import { useHistory } from 'react-router-dom';
import { ClientsContext } from './clientsContext';
import { DnaDialog } from '../../../_Components/Dialog/DnaDialog';
import { DnaButton } from '../../../_Components/Buttons/DnaButton';
import { useTranslation } from 'react-i18next';
import BarChartIcon from '@material-ui/icons/BarChart';
const Wrapper = styled.div<IThemed>`
  display: flex;
  flex-direction: row;
`;
export const Actions = ({ id }) => {
  const { t } = useTranslation();
  const { deleteClient } = useContext(ClientsContext);
  const history = useHistory();
  const [dialogOpen, setDialogOpen] = useState(false);
  const handleDeleteClick = () => {
    deleteClient(id);
  };
  const handleEditClick = () => {
    history.push(`/edit-client/${id}`);
  };

  const handleClose = () => {
    setDialogOpen(false);
  };
  return (
    <Wrapper>
      <DnaIconButton onClick={() => setDialogOpen(true)} iconSize={20}>
        <Delete />
      </DnaIconButton>
      <DnaIconButton onClick={handleEditClick} iconSize={20}>
        <EditIcon />
      </DnaIconButton>
      <DnaIconButton
        onClick={() => history.push(`client/${id}/statistics`)}
        iconSize={20}
      >
        <BarChartIcon />
      </DnaIconButton>
      <DnaDialog
        height={17}
        onClose={handleClose}
        isOpen={dialogOpen}
        title={t('clientList.areYouSure')}
        actions={
          <>
            <DnaButton
              label={t('clientList.yes')}
              onClick={handleDeleteClick}
            />
            <DnaButton label={t('clientList.cancel')} onClick={handleClose} />
          </>
        }
      />
    </Wrapper>
  );
};
