import React, { useEffect, useMemo, useState } from 'react';
import { PageWrapper } from '_Components/PageWrapper';
import { useTranslation } from 'react-i18next';
import { DnaTable } from '_Components/table/DnaTable';
import { IClient } from '_Types/AppTypes';
import { deleteClient, getClients } from '_Api/company.api';
import { Actions } from './Actions';
import { ClientsContext, IClientsContext } from './clientsContext';
import { DnaInput } from '../../../_Components/DnaInput';
import styled from '@emotion/styled';

const StyledInput = styled(DnaInput)`
  margin-bottom: 2rem;
  width: 50%;
`;

const CONFIG = [
  {
    label: 'Name',
    labelKey: 'clientList.name',
    propKey: 'name',
  },
  {
    label: 'Last Name',
    labelKey: 'clientList.lastName',
    propKey: 'lastName',
  },
  {
    label: 'Phone',
    labelKey: 'clientList.phoneNumber',
    propKey: 'phoneNumber',
  },
  {
    label: 'Email',
    labelKey: 'clientList.email',
    propKey: 'email',
  },
  {
    label: 'Birthday',
    labelKey: 'clientList.birthday',
    propKey: 'birthday',
  },
  {
    label: '',
    labelKey: 'clientList.actions',
    key: 'actions',
    className: 'actions-cell',
    cellRender: ({ id }: IClient) => {
      return <Actions id={id} />;
    },
  },
];

export const ClientList = () => {
  const { t } = useTranslation();
  const [clients, setClients] = useState<IClient[]>([]);
  const [filteredClients, setFilteredClients] = useState<IClient[]>([]);
  const [email, setEmail] = useState('');
  const config = useMemo(() => {
    return CONFIG.map((config) => ({
      ...config,
      label: t(config.labelKey),
    }));
  }, [t]);

  const handleEmailChange = ({ target }) => {
    setEmail(target.value);
    const filteredClients = clients.filter((client) =>
      client.email.includes(target.value)
    );
    if (clients !== filteredClients) {
      setFilteredClients(filteredClients);
    }
  };

  const fetchClients = () => {
    getClients().then((clients) => {
      setFilteredClients(clients);
      setClients(clients);
    });
  };

  useEffect(() => {
    fetchClients();
  }, []);

  const context: IClientsContext = {
    clients,
    deleteClient: (clientId?: number) => {
      deleteClient(clientId).then(() => fetchClients());
    },
  };

  return (
    <PageWrapper title={t('clientList.title')} titleText={t('clientList.info')}>
      <ClientsContext.Provider value={context}>
        <StyledInput
          value={email}
          onChange={handleEmailChange}
          label={t('clientList.search')}
        />
        <DnaTable configs={config} data={filteredClients} rowKey={'id'} />
      </ClientsContext.Provider>
    </PageWrapper>
  );
};
