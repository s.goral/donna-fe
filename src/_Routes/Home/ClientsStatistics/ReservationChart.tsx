import React, { useEffect, useState } from 'react';
import { DnaDoughnutChart } from '_Components/Charts/DnaDoughnutChart/DnaDoughnutChart';
import { doughnutOption } from '_Routes/Home/Dashboard/configs';

export const ReservationChart = ({ statistics }) => {
  const [options, setOptions] = useState({});
  useEffect(() => {
    setOptions(
      JSON.parse(
        JSON.stringify(
          doughnutOption(
            statistics?.newReservations,
            statistics?.canceledReservations,
            statistics?.finishedReservations
          )
        )
      )
    );
  }, [statistics]);

  return <DnaDoughnutChart option={options} />;
};
