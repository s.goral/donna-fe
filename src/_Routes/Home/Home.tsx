import React, { FC, useEffect } from 'react';
import styled from '@emotion/styled';
import { NavigationPanel } from '_Routes/NavigationPanel/NavigationPanel';
import { Route } from 'react-router-dom';
import { Dashboard } from './Dashboard/Dashboard';
import { AddClient } from './Client/AddClient/AddClient';
import { ClientList } from '_Routes/Home/ClientList/ClientList';
import { IThemed } from '_Styles/types';
import { Calendar } from '_Routes/Home/Calendar/Calendar';
import { UserList } from './UserList/UserList';
import { EditClient } from './Client/EditClient/EditClient';
import { EmployeeStatistics } from './EmployeeStatistics/EmployeeStatistics';
import { ClientsStatistics } from './ClientsStatistics/ClientsStatistics';
import { Company } from './Company/Company';

const HomeBody = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
`;

const Main = styled.div<IThemed & any>`
  display: flex;
  flex-direction: column;
  position: relative;
  height: 100%;
  width: ${({ isOpen }) =>
    isOpen ? 'calc(100% - 32rem)' : 'calc(100% - 12rem)'};
  background-color: ${({ theme }) => theme.palette.background.default};
`;

export const Home: FC = () => {
  return (
    <HomeBody>
      <NavigationPanel />
      <Main>
        <Route exact path="/dashboard">
          <Dashboard />
        </Route>
        <Route exact path="/add-client">
          <AddClient />
        </Route>
        <Route exact path="/edit-client/:id">
          <EditClient />
        </Route>
        <Route exact path="/clients">
          <ClientList />
        </Route>
        <Route exact path="/calendar">
          <Calendar />
        </Route>
        <Route exact path="/manage-users">
          <UserList />
        </Route>
        <Route exact path="/employee/:employeeId/statistics">
          <EmployeeStatistics />
        </Route>
        <Route exact path="/client/:clientId/statistics">
          <ClientsStatistics />
        </Route>
        <Route exact path="/company">
          <Company />
        </Route>
      </Main>
    </HomeBody>
  );
};
