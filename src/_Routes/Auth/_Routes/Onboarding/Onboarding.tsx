import React, { FC } from 'react';
import { AuthPageHeader, AuthPageWrapper } from '_Routes/Auth/components';
import { useTranslation } from 'react-i18next';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { themeGetters } from '_Styles/themeHelpers';
import { OnboardingForm } from './OnboardingForm';
import { DnaStepButton } from '_Components/Buttons/DnaStepButton/DnaStepButton';

const { spacing } = themeGetters;

const StyledStepButton = styled(DnaStepButton)<IThemed>`
  margin-bottom: ${spacing(7)};
`;

export const Onboarding: FC = () => {
  const { t } = useTranslation();

  return (
    <AuthPageWrapper>
      <StyledStepButton linkTo={'/auth/login'} text={t('auth.goBackToLogin')} />
      <AuthPageHeader>{t('auth.register')}</AuthPageHeader>
      <OnboardingForm />
    </AuthPageWrapper>
  );
};
