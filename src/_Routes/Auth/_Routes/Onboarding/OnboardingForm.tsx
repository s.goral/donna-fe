import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { DnaInput } from '_Components/DnaInput';
import { FieldErrorMessage } from '_Components/Forms/FieldErrorMessage';
import { Form, Formik } from 'formik';
import { prepareRequiredFieldsValidation } from '_Components/Forms/utils';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { DnaButton } from '_Components/Buttons/DnaButton';
import { useHistory, useParams } from 'react-router-dom';
import { onboardUser } from '_Api/auth';

const InputContainer = styled.div<IThemed>`
  display: flex;
  flex-direction: column;

  .dna-input:nth-of-type(2) {
    margin-top: ${({ theme }) => theme.spacing(5)};
  }
`;

const Buttons = styled.div<IThemed>`
  margin-top: ${({ theme }) => theme.spacing(5)};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

const parseJwt = (token) => {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split('')
      .map((c) => {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join('')
  );

  return JSON.parse(jsonPayload);
};

const INITIAL_VALUES = {
  username: '',
  password: '',
  name: '',
  lastName: '',
};

const REQUIRED_FIELDS = ['username', 'password', 'name', 'lastName'];

export const OnboardingForm: FC = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const { token } = useParams();
  const requiredFieldsValidation = prepareRequiredFieldsValidation(
    REQUIRED_FIELDS,
    t('errors.reqField')
  );

  const convertRequest = (form: any) => ({ ...form, token });

  const emailValue = parseJwt(token).email;

  return (
    <Formik
      initialValues={{ ...INITIAL_VALUES, username: emailValue }}
      onSubmit={(form) => {
        if (requiredFieldsValidation(form)) {
          onboardUser(convertRequest(form)).then(() =>
            history.push('/auth/login')
          );
        }
      }}
      validateOnChange={false}
      validateOnBlur={false}
      validate={(form) => {
        return requiredFieldsValidation(form);
      }}
    >
      {({ handleChange, errors }) => (
        <Form>
          <InputContainer>
            <DnaInput
              name="username"
              value={emailValue}
              disabled
              label={t('auth.username')}
              placeholder={t('auth.usernamePlaceholder')}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={''} />
            <DnaInput
              name="password"
              label={t('auth.password')}
              placeholder={t('auth.passwordPlaceholder')}
              type={'password'}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.password} />
            <DnaInput
              name="name"
              label={t('auth.name')}
              placeholder={t('auth.namePlaceholder')}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.name} />
            <DnaInput
              name="lastName"
              label={t('auth.lastName')}
              placeholder={t('auth.lastNamePlaceholder')}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.password} />
            <Buttons>
              <DnaButton inputType="submit" label={t('auth.register')} />
            </Buttons>
          </InputContainer>
        </Form>
      )}
    </Formik>
  );
};
