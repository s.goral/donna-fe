import React, { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DnaInput } from '_Components/DnaInput';
import { FieldErrorMessage } from '_Components/Forms/FieldErrorMessage';
import { Form, Formik } from 'formik';
import { prepareRequiredFieldsValidation } from '_Components/Forms/utils';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { DnaButton } from '_Components/Buttons/DnaButton';
import { registerUser } from '_Api/auth';
import { useHistory } from 'react-router-dom';
import { DnaSelect } from '../../../../_Components/DnaSelect';
import { InputFormWrapper } from '../../../../_Components/Forms/InputFormWrapper';

const InputContainer = styled.div<IThemed>`
  display: flex;
  flex-direction: column;

  .dna-input:nth-of-type(2) {
    margin-top: ${({ theme }) => theme.spacing(5)};
  }
  .service {
    margin-top: ${({ theme }) => theme.spacing(5)};
  }
`;

const StyledSelect = styled(DnaSelect)`
  background-color: white;
`;

const Buttons = styled.div<IThemed>`
  margin-top: ${({ theme }) => theme.spacing(5)};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

const INITIAL_VALUES = {
  username: '',
  password: '',
  name: '',
  lastName: '',
  openingHour: 8,
  closingHour: 20,
  companyName: '',
  services: [],
};

const REQUIRED_FIELDS = [
  'username',
  'password',
  'name',
  'lastName',
  'openingHour',
  'closingHour',
  'companyName',
];

const selectOptions = Array.from(Array(24).keys()).map((value) => {
  return {
    value,
    label: `${value}`,
    id: `${value}`,
  };
});

export const RegisterForm: FC = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const [openingHour, setOpeningHour] = useState(8);
  const [closingHour, setClosingHour] = useState(20);
  const [numChildren, setNumChildren] = useState(1);

  const requiredFieldsValidation = prepareRequiredFieldsValidation(
    REQUIRED_FIELDS,
    t('errors.reqField')
  );

  const handleSelectChange = (name, event, setValue, setter) => {
    const { value } = event.target;
    setter(value);
    setValue(name, value);
  };
  return (
    <Formik
      initialValues={INITIAL_VALUES}
      onSubmit={(form) => {
        if (requiredFieldsValidation(form)) {
          registerUser(form).then(() => history.push('/auth/login'));
        }
      }}
      validateOnChange={false}
      validateOnBlur={false}
      validate={(form) => {
        return requiredFieldsValidation(form);
      }}
    >
      {({ handleChange, errors, setFieldValue }) => (
        <Form>
          <InputContainer>
            <DnaInput
              name="username"
              label={t('auth.username')}
              placeholder={t('auth.usernamePlaceholder')}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.username} />
            <DnaInput
              name="password"
              label={t('auth.password')}
              placeholder={t('auth.passwordPlaceholder')}
              type={'password'}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.password} />
            <DnaInput
              name="name"
              label={t('auth.name')}
              placeholder={t('auth.namePlaceholder')}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.name} />
            <DnaInput
              name="lastName"
              label={t('auth.lastName')}
              placeholder={t('auth.lastNamePlaceholder')}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.lastName} />
            <DnaInput
              name="companyName"
              label={t('auth.companyName')}
              placeholder={t('auth.companyNamePlaceholder')}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.companyName} />
            <StyledSelect
              options={selectOptions}
              label={t('auth.openingHour')}
              value={openingHour}
              onChange={(event) => {
                handleSelectChange(
                  'openingHour',
                  event,
                  setFieldValue,
                  setOpeningHour
                );
              }}
            />
            <FieldErrorMessage message={errors.openingHour} />
            <StyledSelect
              options={selectOptions}
              label={t('auth.closingHour')}
              value={closingHour}
              onChange={(event) => {
                handleSelectChange(
                  'closingHour',
                  event,
                  setFieldValue,
                  setClosingHour
                );
              }}
            />
            <FieldErrorMessage message={errors.closingHour} />
            {Array.from({ length: numChildren }, (_, i) => (
              <>
                <DnaInput
                  onChange={handleChange}
                  placeholder={t('company.servicePlaceholder')}
                  name={`services[${i}]`}
                  label={`${t('company.service')} ${i + 1}`}
                />
                <FieldErrorMessage message={''} />
              </>
            ))}
            <DnaButton
              label={'add'}
              onClick={() => {
                setNumChildren(numChildren + 1);
              }}
            />
            <FieldErrorMessage message={errors.closingHour} />
            <Buttons>
              <DnaButton inputType="submit" label={t('auth.register')} />
            </Buttons>
          </InputContainer>
        </Form>
      )}
    </Formik>
  );
};
