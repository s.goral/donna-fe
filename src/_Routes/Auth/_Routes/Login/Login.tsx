import React, { FC } from 'react';
import { AuthPageHeader, AuthPageWrapper } from '_Routes/Auth/components';
import { useTranslation } from 'react-i18next';
import { LoginForm } from './LoginForm';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { themeGetters } from '_Styles/themeHelpers';
import { DnaButton } from '_Components/Buttons/DnaButton';
import { useHistory } from 'react-router-dom';

const { color, spacing } = themeGetters;

const Divider = styled.div`
  margin-top: 100px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const HorizontalDivider = styled.div<IThemed>`
  height: 1px;
  width: 30%;
  background-color: #c4c4c4;
`;

const SignUpText = styled.div<IThemed>`
  color: ${color('primary', 'contrastText')};
  font-size: ${spacing(2)};
  margin: ${spacing(3, 0)};
  text-align: center;
  width: 40%;
`;

export const Login: FC = () => {
  const { t } = useTranslation();
  const history = useHistory();

  const handleSignUpClick = () => {
    history.push('/auth/register');
  };

  return (
    <AuthPageWrapper>
      <AuthPageHeader>{t('auth.logIn')}</AuthPageHeader>
      <LoginForm />
      <Divider>
        <HorizontalDivider />
        <SignUpText>{t('auth.dontHaveAccount')}</SignUpText>
        <HorizontalDivider />
      </Divider>
      <ButtonWrapper>
        <DnaButton onClick={handleSignUpClick} label={t('auth.signUp')} />
      </ButtonWrapper>
    </AuthPageWrapper>
  );
};
