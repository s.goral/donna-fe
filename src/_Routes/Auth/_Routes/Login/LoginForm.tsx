import React, { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DnaInput } from '_Components/DnaInput';
import { FieldErrorMessage } from '_Components/Forms/FieldErrorMessage';
import { Form, Formik } from 'formik';
import { prepareRequiredFieldsValidation } from '_Components/Forms/utils';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { DnaButton } from '_Components/Buttons/DnaButton';
import { useDispatch } from 'react-redux';
import { login } from '_State/Auth/state';
import { useHistory } from 'react-router-dom';

const InputContainer = styled.div<IThemed>`
  display: flex;
  flex-direction: column;

  .dna-input:nth-of-type(2) {
    margin-top: ${({ theme }) => theme.spacing(5)};
  }
`;

const Buttons = styled.div<IThemed>`
  margin-top: ${({ theme }) => theme.spacing(2)};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

const INITIAL_VALUES = {
  username: '',
  password: '',
};

const REQUIRED_FIELDS = ['username', 'password'];

export const LoginForm: FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [error, setError] = useState('');
  const history = useHistory();
  const requiredFieldsValidation = prepareRequiredFieldsValidation(
    REQUIRED_FIELDS,
    t('errors.reqField')
  );

  const handleForgotClick = () => {
    history.push('/auth/forgot-password');
  };

  return (
    <Formik
      initialValues={INITIAL_VALUES}
      onSubmit={({ password, username }) => {
        dispatch(
          login(
            {
              password,
              username,
            },
            history,
            () => setError(t('auth.badCredentials'))
          )
        );
      }}
      validateOnChange={false}
      validateOnBlur={false}
      validate={(form) => {
        return requiredFieldsValidation(form);
      }}
    >
      {({ handleChange, errors }) => (
        <Form>
          <InputContainer>
            <DnaInput
              name="username"
              label={t('auth.username')}
              placeholder={t('auth.usernamePlaceholder')}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.username} />
            <DnaInput
              name="password"
              label={t('auth.password')}
              placeholder={t('auth.passwordPlaceholder')}
              type={'password'}
              className="dna-input"
              onChange={handleChange}
            />
            <FieldErrorMessage message={errors.password} />
            <FieldErrorMessage message={error} />
            <Buttons>
              <DnaButton inputType="submit" label={t('auth.button')} />
              <DnaButton
                onClick={handleForgotClick}
                label="Forgot my password"
              />
            </Buttons>
          </InputContainer>
        </Form>
      )}
    </Formik>
  );
};
