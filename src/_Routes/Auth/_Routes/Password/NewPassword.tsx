import React, { FC, useState } from 'react';
import { AuthPageHeader, AuthPageWrapper } from '_Routes/Auth/components';
import { useTranslation } from 'react-i18next';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { themeGetters } from '_Styles/themeHelpers';
import { DnaStepButton } from '_Components/Buttons/DnaStepButton/DnaStepButton';
import { DnaInput } from '../../../../_Components/DnaInput';
import { DnaButton } from '../../../../_Components/Buttons/DnaButton';
import { forgotMyPassword } from '../../../../_Api/auth';
import { useHistory, useParams } from 'react-router-dom';
import { changePassword } from '../../../../_Api/company.api';

const { spacing } = themeGetters;

const StyledStepButton = styled(DnaStepButton)<IThemed>`
  margin-bottom: ${spacing(7)};
`;

export const NewPassword: FC = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const [value, setValue] = useState('');
  const { token } = useParams();
  const handleChange = ({ target }) => {
    setValue(target.value);
  };

  const handleClick = () => {
    changePassword({ newPassword: value, token }).then(() => {
      history.push('/auth/login');
    });
  };
  return (
    <AuthPageWrapper>
      <StyledStepButton linkTo={'/auth/login'} text={t('auth.goBackToLogin')} />
      <AuthPageHeader>{'New password'}</AuthPageHeader>
      <DnaInput
        name="username"
        value={value}
        label={t('auth.password')}
        placeholder={t('auth.passwordPlaceholder')}
        type={'password'}
        className="dna-input"
        onChange={handleChange}
      />
      <DnaButton label={'Change'} onClick={handleClick} />
    </AuthPageWrapper>
  );
};
