import React, { FC, useState } from 'react';
import { AuthPageHeader, AuthPageWrapper } from '_Routes/Auth/components';
import { useTranslation } from 'react-i18next';
import { IThemed } from '_Styles/types';
import styled from '@emotion/styled';
import { themeGetters } from '_Styles/themeHelpers';
import { DnaStepButton } from '_Components/Buttons/DnaStepButton/DnaStepButton';
import { DnaInput } from '../../../../_Components/DnaInput';
import { DnaButton } from '../../../../_Components/Buttons/DnaButton';
import { forgotMyPassword } from '../../../../_Api/auth';

const { spacing } = themeGetters;

const StyledStepButton = styled(DnaStepButton)<IThemed>`
  margin-bottom: ${spacing(7)};
`;

export const ForgotPassword: FC = () => {
  const { t } = useTranslation();
  const [value, setValue] = useState('');
  const [info, setInfo] = useState('');
  const handleChange = ({ target }) => {
    setValue(target.value);
  };

  const handleClick = () => {
    forgotMyPassword(value).then(() => {
      setInfo('Email has been sent');
    });
  };
  return (
    <AuthPageWrapper>
      <AuthPageHeader>{'Forgot password'}</AuthPageHeader>
      <StyledStepButton linkTo={'/auth/login'} text={t('auth.goBackToLogin')} />
      <DnaInput
        name="username"
        value={value}
        label={t('auth.username')}
        placeholder={t('auth.usernamePlaceholder')}
        className="dna-input"
        onChange={handleChange}
      />
      <DnaButton label={'Send'} onClick={handleClick} />
    </AuthPageWrapper>
  );
};
