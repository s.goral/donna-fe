import styled from '@emotion/styled';
import { themeGetters } from '_Styles/themeHelpers';
import { IThemed } from '_Styles/types';

const { spacing, color } = themeGetters;

export const AuthPageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  overflow: auto;
  width: 100%;
  height: 100%;
  padding: 5%;
`;

export const AuthPageHeader = styled.div<IThemed>`
  font-size: ${spacing(3.5)};
  color: ${color('primary', 'contrastText')};
  font-weight: 500;
  margin-bottom: ${spacing(4)};
`;
