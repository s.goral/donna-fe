import React, { FC } from 'react';
import styled from '@emotion/styled';
import { Route } from 'react-router-dom';
import { DnaSideImage } from '_Routes/Auth/SideImage/SideImage';
import { Login } from '_Routes/Auth/_Routes/Login/Login';
import { Register } from './_Routes/Register/Register';
import { Onboarding } from '_Routes/Auth/_Routes/Onboarding/Onboarding';
import { ForgotPassword } from './_Routes/Password/ForgotPassword';
import { NewPassword } from './_Routes/Password/NewPassword';

const AuthBody = styled.div`
  height: 100%;
  width: 100%;
  overflow: hidden;
  display: flex;

  .side-image {
    @media (max-width: 650px) {
      display: none;
    }
  }
`;

export const AuthClient: FC = () => {
  return (
    <AuthBody>
      <DnaSideImage />
      <Route exact path="/auth/login">
        <Login />
      </Route>
      <Route exact path="/auth/register">
        <Register />
      </Route>
      <Route exact path="/auth/onboarding/:token">
        <Onboarding />
      </Route>
      <Route exact path="/auth/forgot-password">
        <ForgotPassword />
      </Route>
      <Route exact path="/auth/new-password/:token">
        <NewPassword />
      </Route>
    </AuthBody>
  );
};
