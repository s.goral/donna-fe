import React, { FC } from 'react';
import styled from '@emotion/styled';
import SideImage from './SideImage.png';
import { ICommonProps } from '_Types/props';
import { DnaLogo } from '_Components/DnaLogo/DnaLogo';

const Main = styled.div`
  flex-shrink: 0;
  height: 100%;
  width: 80rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background: url(${SideImage});
`;

const LogoWrapper = styled.div`
  height: 10rem;
  width: 30rem;
  margin: 1rem auto;
`;

export const DnaSideImage: FC<ICommonProps> = ({ className }) => {
  return (
    <Main className={className}>
      <LogoWrapper>
        <DnaLogo />
      </LogoWrapper>
    </Main>
  );
};
