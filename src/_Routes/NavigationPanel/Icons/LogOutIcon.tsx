import React, { FC } from 'react';
interface IProps {
  isFocused: boolean;
}

export const LogOutIcon: FC<IProps> = ({ isFocused }) => {
  return (
    <svg
      width="23"
      height="23"
      viewBox="0 0 23 23"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8.625 20.125H4.79167C4.28334 20.125 3.79582 19.9231 3.43638 19.5636C3.07693 19.2042 2.875 18.7167 2.875 18.2083V4.79167C2.875 4.28334 3.07693 3.79582 3.43638 3.43638C3.79582 3.07693 4.28334 2.875 4.79167 2.875H8.625"
        stroke={isFocused ? '#FFF' : '#fff'}
        strokeWidth="1.4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.3333 16.2918L20.125 11.5002L15.3333 6.7085"
        stroke={isFocused ? '#FFF' : '#fff'}
        strokeWidth="1.4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M20.125 11.5H8.625"
        stroke={isFocused ? '#FFF' : '#FFF'}
        strokeWidth="1.4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
