import React, { FC } from 'react';
import styled from '@emotion/styled';
import { Divider } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setCredentials } from '_Api/api';
import { IThemed } from '_Styles/types';
import { authActions } from '_State/Auth/state';
import { LogOutIcon } from '_Routes/NavigationPanel/Icons/LogOutIcon';
import { MenuIconButton } from '../MenuIconButton';
import i18n from 'i18next';

const Wrapper = styled.div<IThemed>`
  display: flex;
  flex-direction: column;
  margin-bottom: 2.5rem;
  width: 100%;
  margin-top: auto;
`;

const MenuDivider = styled(Divider)<IThemed>`
  && {
    background-color: #9e9e9e;
    height: 2px;
    margin-top: ${({ theme }) => theme.spacing(2.5)};
  }
`;

interface IProps {
  isExtended: boolean;
}

export const Footer: FC<IProps> = ({ isExtended }) => {
  const { t } = useTranslation();
  const history = useHistory();
  const dispatch = useDispatch();

  const language = i18n.language;
  const handleLogoutClick = () => {
    sessionStorage.removeItem('access_token');
    dispatch(authActions.saveCredentials(''));
    setCredentials({ token: undefined });
    history.push('/auth/login');
  };
  return (
    <Wrapper>
      <MenuDivider />
      {isExtended && (
        <>
          <MenuIconButton
            lighter
            onClick={() => i18n.changeLanguage('en')}
            isExtended={isExtended}
            label={t('navBar.eng')}
            isFocused={language === 'en'}
          />
          <MenuIconButton
            lighter
            onClick={() => i18n.changeLanguage('pl')}
            isExtended={isExtended}
            label={t('navBar.pl')}
            isFocused={language === 'pl'}
          />
        </>
      )}
      <MenuIconButton
        lighter
        onClick={handleLogoutClick}
        isExtended={isExtended}
        label={t('navBar.logOut')}
      >
        <LogOutIcon isFocused={false} />
      </MenuIconButton>
    </Wrapper>
  );
};
