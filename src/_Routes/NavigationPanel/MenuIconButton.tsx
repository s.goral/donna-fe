import React, { FC } from 'react';
import {
  createStyles,
  IconButton,
  makeStyles,
  MuiThemeProvider,
  Theme,
} from '@material-ui/core';
import { ICommonProps } from '_Types/props';
import styled from '@emotion/styled';
import { IThemed } from '_Styles/types';
import { useHistory, useLocation } from 'react-router-dom';
import { navigationTheme } from '_Routes/NavigationPanel/styles';

export const buttonStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:hover': {
        background: '#373737',
      },
    },
    expanded: {
      borderRadius: '5px',
      margin: '1rem 2rem 0 2rem;',
      height: '4.5rem',
      display: 'flex',
      alignItems: 'center',
      padding: 0,
      justifyContent: 'start',
    },
    rolled: {
      borderRadius: '5px',
      margin: '1rem 2rem 0 2rem',
      height: '4.5rem',
      display: 'flex',
      alignItems: 'center',
      padding: '0 0 0 0.7rem',
      justifyContent: 'start',
    },
    active: {
      background: '#434343',
      padding: '0.5rem',
      '&:hover': {
        background: '#434343',
      },
    },
  })
);

export const iconStyles = makeStyles(() =>
  createStyles({
    expanded: {
      marginLeft: '1.5rem',
      width: '2.3rem',
      height: '2.3rem',
    },
    rolled: {
      margin: 'auto',
      width: '2.3rem',
      height: '2.3rem',
    },
  })
);

export const labelStyles = makeStyles(() =>
  createStyles({
    lighter: {
      color: '#fff',
      fontWeight: 600,
    },
    active: {
      fontWeight: 600,
    },
  })
);

const Children = styled.div<IThemed>`
  margin-left: ${({ theme }) => theme.spacing(1.5)};
  display: flex;
`;

const Label = styled.div<IThemed>`
  color: ${({ theme }) => theme.palette.background.paper};
  font-size: 1.4rem;
  height: 2rem;
  font-family: Work Sans, sans-serif;
  margin-left: ${({ theme }) => theme.spacing(1.5)};
`;

interface IProps extends ICommonProps {
  isExtended?: boolean;
  label?: string;
  onClick?: () => void;
  isFocused?: boolean;
  linkPath?: string;
  lighter?: boolean;
}

export const MenuIconButton: FC<IProps> = ({
  label,
  isExtended,
  onClick,
  linkPath,
  children,
  className,
  lighter = false,
  isFocused,
}) => {
  const buttonClasses = buttonStyles();
  const iconClasses = iconStyles();
  const labelClasses = labelStyles();
  const history = useHistory();
  const location = useLocation();
  const isActive = location.pathname === linkPath || isFocused;

  const redirectTo = (path: string) => {
    history.push(path);
  };
  const getButtonClass = () => {
    let classes = '';
    if (isActive) {
      classes = `${buttonClasses.active} `;
    }
    const base = isExtended
      ? classes + `${buttonClasses.expanded}`
      : classes + `${buttonClasses.rolled}`;
    return `${base} ${className}`;
  };

  const getLabelClass = () => {
    let classes = '';
    if (isActive) {
      classes = `${labelClasses.active} `;
    }
    if (lighter) {
      classes = `${labelClasses.lighter} `;
    }
    return classes;
  };

  return (
    <MuiThemeProvider theme={navigationTheme}>
      <IconButton
        onClick={linkPath ? () => redirectTo(linkPath) : onClick}
        className={getButtonClass()}
        classes={{ root: buttonClasses.root }}
      >
        <Children
          className={isExtended ? iconClasses.expanded : iconClasses.rolled}
        >
          {children}
        </Children>
        {isExtended && <Label className={getLabelClass()}>{label}</Label>}
      </IconButton>
    </MuiThemeProvider>
  );
};
