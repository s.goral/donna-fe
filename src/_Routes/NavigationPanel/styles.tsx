import {
  createMuiTheme,
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core';

export const navigationTheme = createMuiTheme({
  overrides: {
    MuiSvgIcon: {
      root: {
        color: '#A9B3BB',
      },
    },
  },
});

export const navigationPanelStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      width: '32rem',
      flexShrink: 0,
      whiteSpace: 'nowrap',
      zIndex: 1,
    },
    open: {
      background: theme.palette.primary.dark,
      width: '32rem',
      border: 'none',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      overflowX: 'hidden',
    },
    close: {
      background: theme.palette.primary.dark,
      border: 'none',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: '12rem',
    },
  })
);
