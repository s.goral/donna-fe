import React from 'react';
import { MenuIconButton } from './MenuIconButton';
import { Dashboard, PersonAdd } from '@material-ui/icons';
import PeopleIcon from '@material-ui/icons/People';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import { useTranslation } from 'react-i18next';
import styled from '@emotion/styled';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import BarChartIcon from '@material-ui/icons/BarChart';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
const StyledEmployees = styled(SupervisedUserCircleIcon)`
  font-size: 1.8rem !important;
`;

export const UserButtons = ({ isOpen, isUserAdmin, id }) => {
  const { t } = useTranslation();
  return (
    <>
      <MenuIconButton
        linkPath={'/dashboard'}
        isExtended={isOpen}
        label={t('navBar.dashboard')}
      >
        <Dashboard />
      </MenuIconButton>
      <MenuIconButton
        linkPath={'/add-client'}
        isExtended={isOpen}
        label={t('navBar.addClient')}
      >
        <PersonAdd />
      </MenuIconButton>
      <MenuIconButton
        linkPath={'/clients'}
        isExtended={isOpen}
        label={t('navBar.allClients')}
      >
        <PeopleIcon />
      </MenuIconButton>
      <MenuIconButton
        linkPath={'/calendar'}
        isExtended={isOpen}
        label={t('navBar.calendar')}
      >
        <CalendarTodayIcon />
      </MenuIconButton>
      <MenuIconButton
        linkPath={`/employee/${id}/statistics`}
        isExtended={isOpen}
        label={t('navBar.myStatistics')}
      >
        <BarChartIcon />
      </MenuIconButton>
      {isUserAdmin && (
        <>
          <MenuIconButton
            linkPath={'/manage-users'}
            isExtended={isOpen}
            label={t('navBar.manageEmployees')}
          >
            <StyledEmployees />
          </MenuIconButton>
          <MenuIconButton
            linkPath={'/company'}
            isExtended={isOpen}
            label={t('navBar.company')}
          >
            <AccountBalanceIcon />
          </MenuIconButton>
        </>
      )}
    </>
  );
};
