import React, { FC } from 'react';
import { IconButton, createStyles, makeStyles, Theme } from '@material-ui/core';
import { ICommonProps } from '_Types/props';
import styled from '@emotion/styled';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

interface IProps extends ICommonProps {
  isExtended: boolean;
  onClick?: () => void;
}

export const iconStyles = makeStyles((theme: Theme) =>
  createStyles({
    icon: {
      position: 'absolute',
      background: theme.palette.primary.light,
      borderRadius: '50%',
      boxShadow: '1px 0px 7px rgba(45, 59, 71, 0.19)',
      top: theme.spacing(1.5),
      height: '2.5rem',
      right: '-2.5rem',
      width: '2.5rem',
      display: 'flex',
      alignItems: 'center',
      padding: 0,
      zIndex: 1000,
      ':&hover': {
        zIndex: 1000,
      },
    },
  })
);

const StyledPanelSwitcher = styled.div`
  width: 0;
  z-index: 1500;
`;

const ArrowForward = styled(NavigateNextIcon)`
  transform: rotate(180deg);
`;
const ArrowBack = styled(NavigateNextIcon)`
  transform: none;
`;
export const PanelSwitcher: FC<IProps> = ({ isExtended, onClick }) => {
  const classes = iconStyles();

  return (
    <StyledPanelSwitcher>
      <IconButton
        role="extending-icon"
        className={classes.icon}
        onClick={onClick}
      >
        {isExtended ? <ArrowForward /> : <ArrowBack />}
      </IconButton>
    </StyledPanelSwitcher>
  );
};
