import React, { FC } from 'react';
import { Divider, Drawer } from '@material-ui/core';
import styled from '@emotion/styled';
import { navigationPanelStyles } from '_Routes/NavigationPanel/styles';
import { PanelSwitcher } from '_Routes/NavigationPanel/PanelSwitcher/PanelSwitcher';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { navigationPanelActions } from '_State/NavigationPanel/state';
import { IStateWithNavigationPanel } from '_State/NavigationPanel/model';
import { Footer } from '_Routes/NavigationPanel/Footer/Footer';
import { DnaLogo } from '_Components/DnaLogo/DnaLogo';
import { DnaSmallLogo } from '_Components/DnaLogo/DnaSmallLogo';
import { IThemed } from '_Styles/types';
import { getUserState } from '../../_State/User/state';
import { UserButtons } from './UserButtons';
import { RoleName } from '../../_Types/AppTypes';
import { AdminButtons } from './AdminButtons';

const Wrapper = styled.div`
  display: flex;
  position: relative;
`;
const MenuDivider = styled(Divider)<IThemed>`
  && {
    background-color: #9e9e9e;
    height: 2px;
    margin-top: ${({ theme }) => theme.spacing(2.5)};
  }
`;

const Buttons = styled.div<IThemed>`
  display: flex;
  flex-direction: column;
  margin-top: ${({ theme }) => theme.spacing(4)};
`;

const LogoWrapper = styled.div`
  height: 50px;
  margin-top: 1rem;
`;

export const NavigationPanel: FC = () => {
  const classes = navigationPanelStyles();
  const isOpen = useSelector<IStateWithNavigationPanel, boolean>(
    (state) => state.navigationPanel.isOpen
  );
  const { user } = useSelector(getUserState);

  const isUserAdmin = user?.roleName === RoleName.ROLE_USER_ADMIN;
  const isUser = user?.roleName === RoleName.ROLE_USER || isUserAdmin;
  const isAdmin = user?.roleName === RoleName.ROLE_ADMIN;

  const dispatch = useDispatch();
  const stateClasses = isOpen ? `${classes.open}` : `${classes.close}`;

  const drawerClasses = {
    root: `${classes.drawer} ${stateClasses}`,
    paper: stateClasses,
  };

  const handleDrawerOpen = () => {
    dispatch(navigationPanelActions.toggleOpen());
  };

  return (
    <Wrapper>
      <Drawer variant="permanent" role="drawer" classes={drawerClasses}>
        <LogoWrapper>{isOpen ? <DnaLogo /> : <DnaSmallLogo />}</LogoWrapper>
        <MenuDivider />
        <Buttons>
          {isUser && (
            <UserButtons
              isUserAdmin={isUserAdmin}
              id={user.id}
              isOpen={isOpen}
            />
          )}
          {isAdmin && <AdminButtons isOpen={isOpen} />}
        </Buttons>
        <Footer isExtended={isOpen} />
      </Drawer>
      <PanelSwitcher isExtended={isOpen} onClick={handleDrawerOpen} />
    </Wrapper>
  );
};
