import React from 'react';
import { MenuIconButton } from './MenuIconButton';
import PeopleIcon from '@material-ui/icons/People';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import { useTranslation } from 'react-i18next';
import styled from '@emotion/styled';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';

const StyledEmployees = styled(SupervisedUserCircleIcon)`
  font-size: 1.8rem !important;
`;

export const AdminButtons = ({ isOpen }) => {
  const { t } = useTranslation();
  return (
    <>
      <MenuIconButton
        linkPath={'/admin/companies'}
        isExtended={isOpen}
        label={t('navBar.allCompanies')}
      >
        <AccountBalanceIcon />
      </MenuIconButton>
      <MenuIconButton
        linkPath={'/admin/clients'}
        isExtended={isOpen}
        label={t('navBar.allClients')}
      >
        <PeopleIcon />
      </MenuIconButton>
      <MenuIconButton
        linkPath={'/admin/manage-users'}
        isExtended={isOpen}
        label={t('navBar.manageUsers')}
      >
        <StyledEmployees />
      </MenuIconButton>
    </>
  );
};
