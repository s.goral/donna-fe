import { donnaApi } from '_Api/api';
import { IUser } from '../_State/User/types';

interface IRegisterUserPayload {
  username: string;
  password: string;
  name: string;
  lastName: string;
  companyName: string;
  token?: string;
}

export interface IUserLoginPayload {
  username: string;
  password: string;
}

export interface IUserLoginResponse {
  token: string;
}

export const registerUser = (payload: IRegisterUserPayload) => {
  return donnaApi
    .post('auth/register', {
      json: payload,
    })
    .json();
};

export const onboardUser = (payload: IRegisterUserPayload) => {
  return donnaApi
    .post('auth/onboard', {
      json: payload,
    })
    .json();
};
export const getDetails = (): Promise<IUser> => {
  return donnaApi.get('auth/user-details').json();
};

export const forgotMyPassword = (email): Promise<any> => {
  return donnaApi
    .post('auth/forgot', {
      headers: {
        Accept: 'application/json',
      },
      body: email,
    })
    .json();
};

export const loginUser = ({ password, username }: IUserLoginPayload) => {
  return donnaApi
    .post('auth/login', {
      headers: {
        Accept: 'application/json',
      },
      json: {
        username: username,
        password: password,
      },
    })
    .json<IUserLoginResponse>();
};
