import { donnaApi } from '_Api/api';
import { IClient, ICompany, IUser } from '../_Types/AppTypes';

export const getClients = (): Promise<IClient[]> => {
  return donnaApi.get('admin/clients').json();
};

export const deleteAdminClient = (id): Promise<any> => {
  return donnaApi.delete(`admin/client/${id}`);
};

export const deleteAdminUser = (id): Promise<any> => {
  return donnaApi.delete(`admin/delete/user/${id}`);
};

export const updateAdminRoleName = (roleName, id) => {
  return donnaApi.patch(`admin/user/role/${id}`, {
    json: roleName,
  });
};

export const getUsers = (): Promise<IUser[]> => {
  return donnaApi.get('admin/employees').json();
};
export const getCompanies = (): Promise<ICompany[]> => {
  return donnaApi.get('admin/companies').json();
};

export const deleteCompany = (id): Promise<any> => {
  return donnaApi.delete(`admin/delete/company/${id}`);
};

export const getAdminClient = (id): Promise<IClient> => {
  return donnaApi.get(`admin/client/${id}`).json();
};

export const updateAdminClient = (clientForm, id): Promise<any> => {
  return donnaApi.put(`admin/edit-client/${id}`, {
    body: JSON.stringify(clientForm),
  });
};
