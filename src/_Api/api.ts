import ky from 'ky';

export interface IApiCredentials {
  token?: string;
}

const credentials: IApiCredentials = {
  token: undefined,
};

export const setCredentials = (newCredentials: IApiCredentials) => {
  Object.assign(credentials, newCredentials);
};

const authInterceptor = (request: Request) => {
  if (credentials.token) {
    request.headers.set('Authorization', `Bearer ${credentials.token}`);
  }
  return request;
};

export const donnaApi = ky.create({
  prefixUrl: '/',
  hooks: {
    beforeRequest: [authInterceptor],
  },
  headers: {
    'content-type': 'application/json',
  },
});
