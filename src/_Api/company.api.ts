import { donnaApi } from '_Api/api';
import {
  IStatistics,
  IClient,
  IReservationDTO,
  IEmployee,
  IUser,
  IPersonData,
} from '_Types/AppTypes';

export const getStatistics = (): Promise<IStatistics> => {
  return donnaApi.get('client-company/statistics').json();
};

export const getClients = (): Promise<IClient[]> => {
  return donnaApi.get('client-company/clients').json();
};

export const getUsers = (): Promise<IUser[]> => {
  return donnaApi.get('client-company/users').json();
};

export const getEmployees = (): Promise<IEmployee[]> => {
  return donnaApi.get('client-company/employees').json();
};

export const addNewClient = (clientForm): Promise<any> => {
  return donnaApi.post('client-company/add-client', {
    body: JSON.stringify(clientForm),
  });
};

export const getClient = (id): Promise<IClient> => {
  return donnaApi.get(`client-company/client/${id}`).json();
};

export const deleteClient = (id): Promise<any> => {
  return donnaApi.delete(`client-company/client/${id}`);
};

export const deleteUser = (id): Promise<any> => {
  return donnaApi.delete(`client-company/delete/user/${id}`);
};

export const updateRoleName = (roleName, id) => {
  return donnaApi.patch(`client-company/user/role/${id}`, {
    json: roleName,
  });
};

export const updateClient = (clientForm, id): Promise<any> => {
  return donnaApi.put(`client-company/edit-client/${id}`, {
    body: JSON.stringify(clientForm),
  });
};

export const inviteUsers = (users): Promise<any> => {
  return donnaApi.post('client-company/invite-users', {
    body: JSON.stringify(users),
  });
};

export const updateCompany = (form): Promise<any> => {
  return donnaApi.put('client-company/edit-company', {
    json: form,
  });
};

export const addNewReservation = (reservationForm): Promise<any> => {
  return donnaApi.post('client-company/add-reservation', {
    json: reservationForm,
  });
};

export const editReservation = (reservationForm, id: number): Promise<any> => {
  return donnaApi.put(`client-company/edit-reservation/${id}`, {
    json: reservationForm,
  });
};

export const findReservation = (time, employeeId): Promise<any> => {
  return donnaApi
    .post(`client-company/next-reservation/user/${employeeId}`, {
      json: time,
    })
    .json();
};

export const changePassword = (changePasswordDto) => {
  return donnaApi.put('auth/change-password', {
    json: changePasswordDto,
  });
};

export const cancelReservation = (id: number): Promise<any> => {
  return donnaApi.post(`client-company/archive-reservation/${id}`);
};
export const getReservations = (): Promise<any> => {
  return donnaApi.get('client-company/reservations').json();
};

export const getEmployeeData = (id): Promise<IPersonData> => {
  return donnaApi.get(`client-company/employee/data/${id}`).json();
};

export const getCompanyData = (): Promise<any> => {
  return donnaApi.get('client-company/company').json();
};

export const getClientData = (id): Promise<IPersonData> => {
  return donnaApi.get(`client-company/client/data/${id}`).json();
};
